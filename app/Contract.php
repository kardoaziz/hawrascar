<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    //
    public function gumrg() {
        return $this->belongsTo('App\Gumrg','id_gumrg');
    }
}
