<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gumrg extends Model
{
    //
    public function Customers() {
        return $this->belongsTo('App\Customers','customer_id');
    }
    public function marz() {
        return $this->belongsTo('App\Marz','id_marz');
    }
    public function car() {
        return $this->belongsTo('App\Cars','car_name');
    }
    public function colored() {
        return $this->belongsTo('App\Colors','color');
    }
    public function Gumrg()
    {
        return $this->hasMany('Contract');
    }
}
