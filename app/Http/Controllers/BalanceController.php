<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Balance;
use App\Marz;
class BalanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $balance=Balance::paginate(4);
        $i=0;
        return view('balance.balance',['balance'=>$balance,'i'=>$i]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $obj = new Balance();
        $obj->qty=$request->input('qty');
        $obj->unit_price=$request->input('unit_price');
        $obj->amount=$request->input('amount');
        $obj->date=$request->input('date');
        $obj->invoice_no=$request->input('invoice_no');
        $obj->note=$request->input('note');
        $obj->id_marz=$request->input('id_marz');
        $obj->save();
        $balance=Balance::where('id_marz','=',$obj->id_marz)->paginate(10);
        $i=0;
        return view('marz.detail',['balance'=>$balance,'i'=>$i,'id_marz'=>$obj->id_marz]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $balance=Balance::where('id_marz','=',$id)->paginate(10);
        $i=0;
        $marz = Marz::find($id);
        return view('marz.detail',['balance'=>$balance,'i'=>$i,'marz'=>$marz,'id_marz'=>$id]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $balance = Balance::find($id);
        return view('marz.edit',['balance'=>$balance,'id_marz'=>$id]);
        // $obj=Balance::find($id);
        // return view('marz.edit',['balance'=>$obj]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $obj=Balance::find($id);
        $obj->amount= $request->input("amount");
        $obj->date= $request->input("date");
        $obj->qty= $request->input("qty");
        $obj->unit_price= $request->input("unit_price");
        $obj->invoice_no= $request->input("invoice_no");
        $obj->note= $request->input("note");
        $obj->save();

        $balance=Balance::where('id_marz','=',$obj->id_marz)->paginate(10);
        $i=0;
        return view('marz.detail',['balance'=>$balance,'i'=>$i,'id_marz'=>$id]);
}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         error_log('Some message here.');
         $obj=Balance::find($id);
         $id_marz = $obj->id_marz;
        $balance = Balance::findOrFail($id);
        $balance->delete();
        \Session::flash('flash_message_delete','Office successfully deleted.');
        $obj=Balance::find($id);
        $balance=Balance::where('id_marz','=',$id_marz)->paginate(10);
        $i=0;
        return view('marz.detail',['balance'=>$balance,'i'=>$i,'id_marz'=>$id_marz]);
    }
}
