<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gumrg;
use App\Customers;
use App\Contract;
use App\Colors;
use App\Cars;
use DB;
use App\Marz;
class GumrgController extends Controller
{

    public function __construct() {
        $this->middleware(['auth', 'isAdmin']);//isAdmin middleware lets only users with a //specific permission permission to access these resources
    }
//
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $obj = Gumrg::paginate(10);
        $customers = Customers::all();
        $colors = Colors::all();
        $cars = Cars::all();
        $marz = Marz::all();
        foreach ($marz as $m) {
            # code...
        $balances = DB::table('balances')->where('id_marz','=',$m->id)->sum('qty');
        $gumrgs = Gumrg::where('id_marz','=',$m->id);
        if($gumrgs)
            $m->total=$gumrgs->count();
        else
            $m->total=0;
       
        if($balances)
            {$m->balance=$balances;}
        else
            {$m->balance=0;}
         }

        $total= array("transfer_fee"=>0,"total"=>0,"gumrg"=>0,"balance"=>0);
         foreach($obj as $c) {
            $contract = Contract::where('id_gumrg','=',$c->id)->get();

            $total['transfer_fee'] += $c->transfer_fee;
            $total['total'] += $c->total;
            $total['gumrg'] += $c->gumrg;
            $total['balance'] += $c->balance;
            if(sizeof($contract)>0)
            {
                $contract=$contract[0];
                $c->contract_no = $contract->id;
                $c->export_date = $contract->date; 
            }
           
        }
        $i=0;
        // $total = json_encode($total, JSON_FORCE_OBJECT);
        return view('Gumrg.Gumrg',['i'=>$i,'gumrg'=>$obj,"customers"=>$customers,"colors"=>$colors,"cars"=>$cars,"total"=>$total,'marz'=>$marz,'selectedC'=>"","fdate"=>'',"tdate"=>'']);
    }
    public function search_customer(Request $request)
    {
        //
        $customers = Customers::all();
        $colors = Colors::all();
        $cars = Cars::all();
        $marz = Marz::all();
        foreach ($marz as $m) {
            # code...
        $balances = DB::table('balances')->where('id_marz','=',$m->id)->sum('qty');
        $gumrgs = Gumrg::where('id_marz','=',$m->id);
        if($gumrgs)
            $m->total=$gumrgs->count();
        else
            $m->total=0;
       
        if($balances)
            {$m->balance=$balances;}
        else
            {$m->balance=0;}
         }

        $search = $request->input("search");
        $obj  = Gumrg::Where('customer_id','=',$search);
        if(sizeof($request->input("from_date"))>0 && sizeof($request->input("till_date"))>0)
        {
            $obj=$obj->where('import_date','>=',$request->input("from_date"));
            $obj=$obj->where('import_date','<=',$request->input("till_date"));
        }
        $obj= $obj->paginate(10);
         $total= array("transfer_fee"=>0,"total"=>0,"gumrg"=>0,"balance"=>0);
         foreach($obj as $c) {
            $contract = Contract::where('id_gumrg','=',$c->id)->get();

            $total['transfer_fee'] += $c->transfer_fee;
            $total['total'] += $c->total;
            $total['gumrg'] += $c->gumrg;
            $total['balance'] += $c->balance;
            if(sizeof($contract)>0)
            {
                $contract=$contract[0];
                $c->contract_no = $contract->id;
                $c->export_date = $contract->date; 
            }
           
        }
        $i=0;
         return view('Gumrg.Gumrg',['i'=>$i,'gumrg'=>$obj,"customers"=>$customers,"colors"=>$colors,"total"=>$total,"cars"=>$cars,'selectedC'=>$search,'fdate'=>$request->input("from_date"),'tdate'=>$request->input("till_date"),'marz'=>$marz]);
    }

    public function search(Request $request)
    {
        //
                $customers = Customers::all();
        $colors = Colors::all();
        $cars = Cars::all();
        $marz = Marz::all();
        foreach ($marz as $m) {
            # code...
        $balances = DB::table('balances')->where('id_marz','=',$m->id)->sum('qty');
        $gumrgs = Gumrg::where('id_marz','=',$m->id);
        if($gumrgs)
            $m->total=$gumrgs->count();
        else
            $m->total=0;
       
        if($balances)
            {$m->balance=$balances;}
        else
            {$m->balance=0;}
         }

        $search = $request->input("search");
        $obj  = Gumrg::Where('year','like','%'.$search.'%')
        ->orWhere('vin','like','%'.$search.'%')
        ->orWhere('balance','=',$search)
        ->orWhere('transfer_fee','=',$search)
        ->orWhere('temp_plate','like','%'.$search.'%')
        ->orWhere('specification','like','%'.$search.'%')
        ->orWhere('cylinder','=',$search)
        ->orWhere('total','=',$search)
        ->orWhere('gumrg','=',$search);
        if(sizeof($request->input("from_date"))>0 && sizeof($request->input("till_date"))>0)
        {
            $obj=$obj->where('import_date','>=',$request->input("from_date"));
            $obj=$obj->where('import_date','<=',$request->input("till_date"));
        }
        $obj= $obj->paginate(10);
         $total= array("transfer_fee"=>0,"total"=>0,"gumrg"=>0,"balance"=>0);
         foreach($obj as $c) {
            $contract = Contract::where('id_gumrg','=',$c->id)->get();

            $total['transfer_fee'] += $c->transfer_fee;
            $total['total'] += $c->total;
            $total['gumrg'] += $c->gumrg;
            $total['balance'] += $c->balance;
            if(sizeof($contract)>0)
            {
                $contract=$contract[0];
                $c->contract_no = $contract->id;
                $c->export_date = $contract->date; 
            }
           
        }
        $i=0;
         return view('Gumrg.Gumrg',['i'=>$i,'gumrg'=>$obj,"customers"=>$customers,"colors"=>$colors,"total"=>$total,"cars"=>$cars,'selectedC'=>$search,'fdate'=>$request->input("from_date"),'tdate'=>$request->input("till_date"),'marz'=>$marz]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $obj = new Gumrg();
        $obj->car_name=$request->input('car_name');
        $obj->color=$request->input('color');
        $obj->vin=$request->input('vin');
        $obj->year=$request->input('year');
        $obj->specification=$request->input('specification');
        $obj->cylinder=$request->input('cylinder');
        $obj->temp_plate=$request->input('temp_plate');
        $obj->gumrg=$request->input('gumrg');
        $obj->balance=$request->input('balance');
        $obj->transfer_fee=$request->input('transfer_fee');
        $obj->total=$request->input('total');
        $obj->import_date=$request->input('import_date');
        $obj->export_date=$request->input('export_date');
        $obj->contract_no=$request->input('contract_no');
        $obj->customer_id=$request->input('customer_id');
        $obj->id_marz=$request->input('id_marz');
        $obj->save();
        return redirect(route('gumrgs.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $obj = Gumrg::find($id);
        $contract = Contract::where('id_gumrg','=',$id)->first();
        return view('Gumrg.add_contract',['gumrg'=>$obj,'contract'=>$contract]);
    }
    public function showContract(Request $request)
    {
        //
        $obj = Gumrg::find($request->input('id_gumrg'));
        $contract = Contract::where('id_gumrg','=',$request->input('id_gumrg'))->first();
        return view('Gumrg.contract',['gumrg'=>$obj,'contract'=>$contract]);
    }
    public function insertContract(Request $request)
    {
        //
        $obj = Contract::where('id_gumrg','=',$request->id_gumrg)->first();
        if(sizeof($obj)>0)
        {
        //     //update
        $obj =  Contract::find($obj->id);
        $obj->buyer=$request->input('buyer');
        $obj->phone=$request->input('phone');
        $obj->date=$request->input('date');
        $obj->address=$request->input('address');
        $obj->attach_no=$request->input('attach_no');
        $obj->attach_date=$request->input('attach_date');
        $obj->id_gumrg=$request->input('id_gumrg');
        $obj->save();
        }
        else{
            //insert
            $obj = new Contract();
        $obj->buyer=$request->input('buyer');
        $obj->phone=$request->input('phone');
        $obj->date=$request->input('date');
        $obj->address=$request->input('address');
        $obj->attach_no=$request->input('attach_no');
        $obj->attach_date=$request->input('attach_date');
        $obj->id_gumrg=$request->input('id_gumrg');
        $obj->save();
        // return redirect(route('gumrgs.index'));

        }
        $obj = Gumrg::find($request->input('id_gumrg'));
        $contract = Contract::where('id_gumrg','=',$request->input('id_gumrg'))->first();
        return view('Gumrg.add_contract',['gumrg'=>$obj,'contract'=>$contract]);    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $gumrg = Gumrg::find($id);
         $customers = Customers::all();
         $cars = Cars::all();
         $colors = Colors::all();
         $marz = Marz::all();
         foreach ($marz as $m) {
            # code...
        $balances = DB::table('balances')->where('id_marz','=',$m->id)->sum('qty');
        $gumrgs = Gumrg::where('id_marz','=',$m->id);
        if($gumrgs)
            $m->total=$gumrgs->count();
        else
            $m->total=0;
       
        if($balances)
            {$m->balance=$balances;}
        else
            {$m->balance=0;}
         }

        return view('Gumrg.edit',['gumrg'=>$gumrg,'customers'=>$customers,'cars'=>$cars,'colors'=>$colors,'marz'=>$marz]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // return view('Gumrg.edit')
         $obj=Gumrg::find($id);
        $obj->contract_no= $request->input("contract_no");
        $obj->export_date= $request->input("export_date");
        $obj->import_date= $request->input("import_date");
        $obj->total= $request->input("total");
        $obj->transfer_fee= $request->input("transfer_fee");
        $obj->balance= $request->input("balance");
        $obj->gumrg= $request->input("gumrg");
        $obj->temp_plate= $request->input("temp_plate");
        $obj->specification= $request->input("specification");
        $obj->cylinder= $request->input("cylinder");
        $obj->year= $request->input("year");
        $obj->color= $request->input("color");
        $obj->car_name= $request->input("car_name");
        $obj->customer_id= $request->input("customer_id");
        $obj->id_marz= $request->input("id_marz");
        $obj->save();
        return redirect(route('gumrgs.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         //
        error_log('Some message here.');
        $gumrg = Gumrg::findOrFail($id);
        $gumrg->delete();
        \Session::flash('flash_message_delete','Office successfully deleted.');

        // $output = new \Symfony\Component\Console\Output\ConsoleOutput(2);
        // Log::info('This is some useful information.');

        // $output->writeln('hello');
        return redirect(route('gumrgs.index'));


    }

    public function naql(){
        $obj = Gumrg::paginate(10);
        $customers = Customers::all();
        $colors = Colors::all();
        $cars = Cars::all();
        $marz = Marz::all();
        foreach ($marz as $m) {
            # code...
        $balances = DB::table('balances')->where('id_marz','=',$m->id)->sum('qty');
        $gumrgs = Gumrg::where('id_marz','=',$m->id);
        if($gumrgs)
            $m->total=$gumrgs->count();
        else
            $m->total=0;
       
        if($balances)
            {$m->balance=$balances;}
        else
            {$m->balance=0;}
         }

        $total= array("transfer_fee"=>0,"total"=>0,"gumrg"=>0,"balance"=>0);
        foreach($obj as $c) {
            $total['transfer_fee'] += $c->transfer_fee;
            $total['total'] += $c->total;
            $total['gumrg'] += $c->gumrg;
            $total['balance'] += $c->balance;
        }
        $i=0;
        // $total = json_encode($total, JSON_FORCE_OBJECT);
        return view('naql.index',['i'=>$i,'gumrg'=>$obj,"customers"=>$customers,"colors"=>$colors,"cars"=>$cars,"total"=>$total,'marz'=>$marz]);    
    }


        function action(Request $request)
    {
     if($request->ajax())
     {
      $output = '';
      $query = $request->get('query');
      if($query != '')
      {
       $data = DB::table('gumrgs')
         ->where('color', 'like', '%'.$query.'%')
         ->orWhere('gumrg', 'like', '%'.$query.'%')
         ->orWhere('car_name', 'like', '%'.$query.'%')
         ->orWhere('year', 'like', '%'.$query.'%')
         ->orWhere('specification', 'like', '%'.$query.'%')
         ->orWhere('vin', 'like', '%'.$query.'%')
         ->orWhere('temp_plate', 'like', '%'.$query.'%')
         ->orWhere('balance', 'like', '%'.$query.'%')
         ->orWhere('cylinder', '=', $query)
         ->orWhere('transfer_fee', 'like', '%'.$query.'%')
         ->orWhere('total', 'like', '%'.$query.'%')
         ->orWhere('import_date', 'like', '%'.$query.'%')
         ->orWhere('export_date', 'like', '%'.$query.'%')
         ->orWhere('contract_no', 'like', '%'.$query.'%')
         ->orderBy('id', 'desc')->paginate(10);
         
      }
      else
      {
       $data = DB::table('gumrgs')
         ->orderBy('id', 'desc')
         ->paginate(10);
      }
      $total_row = $data->count();
      if($total_row > 0)
      {
        $i=0;
       foreach($data as $row)

       {
        $cu_name=Customers::find($row->customer_id);
        $output .= '

        <tr>
         <td>'.$row->contract_no.'</td>
         <td>'.$row->export_date.'</td>
         <td>'.$row->import_date.'</td>
         <td>'.$row->total.'</td>
         <td>'.$row->transfer_fee.'</td>
         <td>'.$row->balance.'</td>
         <td>'.$row->gumrg.'</td>
         <td>'.$row->temp_plate.'</td>
         <td>'.$row->vin.'</td>
         <td>'.$row->specification.'</td>
         <td>'.$row->year.'</td>
         <td>'.$row->color.'</td>
         <td>'.$row->car_name.'</td>
         <td>'.$cu_name->name.'</td>
         <td>'.(++$i).'</td>
         <td><a href="gumrgs/'.$row->id.'/edit"><button >Edit</button>
</td>
        </tr>
        ';
       }
      }
      else
      {
       $output = '
       <tr>
        <td align="center" colspan="5">No Data Found</td>
       </tr>

       ';
      }
      $data = array(
       'table_data'  => $output,
       'total_data'  => $total_row
      );

      echo json_encode($data);
     }
    }
}
