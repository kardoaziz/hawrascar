<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\gumrgpayment;
use App\Marz;
class GumrgPaymenyController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $obj = new gumrgpayment();
        $obj->amount=$request->input('amount');
        $obj->date=$request->input('date');
        $obj->invoice_no=$request->input('invoice_no');
        $obj->note=$request->input('note');
        $obj->id_marz=$request->input('id_marz');
        $obj->save();
        $payment=gumrgpayment::where('id_marz','=',$obj->id_marz)->paginate(10);
        $i=0;
        return view('marz.gumrg',['payment'=>$payment,'i'=>$i,'id_marz'=>$obj->id_marz]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $payment=gumrgpayment::where('id_marz','=',$id)->paginate(10);
        $i=0;
        $marz = Marz::find($id);
        return view('marz.gumrg',['payment'=>$payment,'marz'=>$marz,'i'=>$i,'id_marz'=>$id]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $payment = gumrgpayment::find($id);
        return view('marz.editgumrg',['payment'=>$payment,'id_marz'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $obj=gumrgpayment::find($id);
        $obj->amount= $request->input("amount");
        $obj->date= $request->input("date");
        $obj->invoice_no= $request->input("invoice_no");
        $obj->note= $request->input("note");
        $obj->save();

        $payment=gumrgpayment::where('id_marz','=',$obj->id_marz)->paginate(10);
        $i=0;
        return view('marz.gumrg',['payment'=>$payment,'i'=>$i,'id_marz'=>$id]);

        //
        }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         $obj=gumrgpayment::find($id);
         $id_marz = $obj->id_marz;
        $payment = gumrgpayment::findOrFail($id);
        $payment->delete();
        \Session::flash('flash_message_delete','Office successfully deleted.');
        $payment=gumrgpayment::where('id_marz','=',$id_marz)->paginate(10);
        $i=0;
        return view('marz.gumrg',['payment'=>$payment,'i'=>$i,'id_marz'=>$id_marz]);
    }
}
