<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Marz;
use App\Balance;
use App\Gumrg;
use DB;
class MarzController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $marz=Marz::all();
        $i=0;
        foreach ($marz as $m) {
            # code...
        $balance = DB::table('balances')->where('id_marz','=',$m->id)->sum('qty');
        $payment = DB::table('gumrgpayments')->where('id_marz','=',$m->id)->sum('amount');
        $qarzgumrg = DB::table('gumrgs')->where('id_marz','=',$m->id)->sum('gumrg');
        $gumrg = Gumrg::where('id_marz','=',$m->id);
        if($gumrg)
            $m->total=$gumrg->count();
        else
            $m->total=0;
       
        if($balance)
            {$m->balance=$balance;}
        else
            {$m->balance=0;}
         
         if($payment)
            {$m->payment=$payment;}
        else
            {$m->payment=0;}
         
         if($qarzgumrg)
            {$m->qarzgumrg=$qarzgumrg;}
        else
            {$m->qarzgumrg=0;}
         }

        return view('marz.marz',['marz'=>$marz,'i'=>$i]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $obj = new Marz();
        $obj->name=$request->input('name');
        $obj->save();
        return redirect(route('marz.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       
         $marz = Marz::find($id);
        return view('marz.editmarz',['marz'=>$marz]);
    }

    // public function editmarz($id)
    // {
    //     //
    //     $marz = Marz::find($id);
    //     // return redirect(route('marz.edit'),['marz'=>$marz]);

    // }
    public function balance($id)
    {
        //
        $balance = Balance::where('id_marz','=',$id)->get();
        $marz = Marz::find($id);
        return view('marz.detail',['balance'=>$balance,'marz'=>$marz,'id_marz'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         //
         $obj=Marz::find($id);
        $obj->name= $request->input("name");
      
        $obj->save();
        return redirect(route('marz.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
