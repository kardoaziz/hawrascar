<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payments;
use App\Customers;
use DB;
class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $customers = Customers::all();
        $payments = Payments::all();
        $i=0;
        // $pay = Payments::all()
        //     ->join('Customers','customers.id','=','payments.customer_id')
        //     ->groupBy('customers.name')
        //     ->map(function ($m) {
        //         return $m->sum('amount');
        //     });
        $pay = DB::table('payments')
       ->join('customers', 'customers.id', '=', 'payments.customer_id')
       ->select('customers.name','customers.id', DB::raw('sum(payments.amount) as total '))
       ->groupBy(['customers.name','customers.id'])
       ->get();

       $gumrg = DB::table('gumrgs')
       ->join('customers', 'customers.id', '=', 'gumrgs.customer_id')
       ->select('customers.name','customers.id', DB::raw('sum(gumrgs.total) as total '))
       ->groupBy(['customers.name','customers.id'])
       ->get();
       foreach($gumrg as $g)
       {
        foreach ($pay as $p) {
            if($p->id == $g->id)
            {
                $g->pay= $p->total;
            }
            # code...
        }
       }
        return view('payments.payment',['customers'=>$customers, 'payments'=>$payments,'i'=>$i,'pay'=>$pay,'gumrg'=>$gumrg]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $obj = new Payments();
        $obj->amount=$request->input('amount');
        $obj->note=$request->input('note');
        $obj->date=$request->input('date');
        $obj->customer_id=$request->input('customer_id');
        $obj->save();
        return redirect(route('payments.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         $payment = Payments::where('customer_id','=',$id)->get();
         $customers = Customers::all();
         $customer = Customers::find($id);
         $i=0;
         return view('payments.detail',['payment'=>$payment,'customers'=>$customers,'customer'=>$customer,'i'=>$i]);
    }
    public function search(Request $request)
    {
        //
       $customers = Customers::all();
        $payments = Payments::all();
        $i=0;

        $pay = DB::table('payments')
       ->join('customers', 'customers.id', '=', 'payments.customer_id')
       ->select('customers.name','customers.id', DB::raw('sum(payments.amount) as total '))
       ->groupBy(['customers.name','customers.id'])
       ->get();

       $selected=$request->input("search");
       $gumrg = DB::table('gumrgs')
       ->join('customers', 'customers.id', '=', 'gumrgs.customer_id')
       ->select('customers.name','customers.id', DB::raw('sum(gumrgs.total) as total '))
       ->groupBy(['customers.name','customers.id'])
       ->having('customers.id','=',$selected)
       ->get();
       foreach($gumrg as $g)
       {
        foreach ($pay as $p) {
            if($p->id == $g->id)
            {
                $g->pay= $p->total;
            }
            # code...
        }
       }
        return view('payments.payment',['customers'=>$customers, 'payments'=>$payments,'i'=>$i,'pay'=>$pay,'gumrg'=>$gumrg,'selected'=>$selected]);
            }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $payment = Payments::find($id);
        $customers = Customers::all();
        return view('payments.edit',['payment'=>$payment,'customers'=>$customers]);
    }

    public function invoice($id)
    {
        //
         $payment = Payments::find($id);
        $customer = Customers::find($payment->customer_id);
        return view('payments.invoice',['payment'=>$payment,'customer'=>$customer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $obj=Payments::find($id);
        $obj->amount= $request->input("amount");
        $obj->date= $request->input("date");
        $obj->note= $request->input("note");
        $obj->customer_id= $request->input("customer_id");
        $obj->save();
        return redirect(route('payments.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
          error_log('Some message here.');
        $gumrg = Payments::findOrFail($id);
        $gumrg->delete();
        \Session::flash('flash_message_delete','Payment successfully deleted.');

        return redirect(route('payments.index'));

    }
}
