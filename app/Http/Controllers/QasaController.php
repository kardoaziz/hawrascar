<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Qasa;
use App\XawanQasa;
use DB;
class QasaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $qasa = Qasa::all();
        $xawanQasa=XawanQasa::all();
        $i=0;
        $in = DB::table('qasas')
        ->join('xawan_qasas', 'xawan_qasas.id', '=', 'qasas.id_xawan_qasa')
        ->select('xawan_qasas.name','xawan_qasas.id', DB::raw('COALESCE(sum(qasas.amount),0) as total'))->where('qasas.in_out','like','IN')
        ->groupBy(['xawan_qasas.name','xawan_qasas.id'])
        ->get();
        $out = DB::table('qasas')
        ->join('xawan_qasas', 'xawan_qasas.id', '=', 'qasas.id_xawan_qasa')
        ->select('xawan_qasas.name','xawan_qasas.id', DB::raw('COALESCE( sum(qasas.amount),0) as total'))->where('qasas.in_out','like','OUT')
        ->groupBy(['xawan_qasas.name','xawan_qasas.id'])
        ->get();

        $inQasa=Qasa::where('in_out','like','IN')->sum('amount');
        $outQasa=Qasa::where('in_out','like','OUT')->sum('amount');

        foreach($xawanQasa as $xq)
        {
            foreach($in as $ins)
            {
                if($xq->id==$ins->id)
                    $xq->intotal = $ins->total;
            }
            if(empty($xq->intotal))
            $xq->intotal=0;
            foreach($out as $outs)
            {
                if($xq->id==$outs->id)
                    $xq->outtotal = $outs->total;
            }
            if(empty($xq->outtotal))
            $xq->outtotal=0;
        }

        return view('qasa.index',['qasa'=>$qasa,'xawanQasa'=>$xawanQasa,'in'=>$in,'out'=>$out,'i'=>$i,'inQasa'=>$inQasa,'outQasa'=>$outQasa]);
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $obj = new Qasa();
        $obj->amount=$request->input('amount');
        $obj->id_xawan_qasa=$request->input('id_xawan_qasa');
        $obj->note=$request->input('note');
        $obj->date=$request->input('date');
        $obj->in_out=$request->input('in_out');
        $obj->save();
        return redirect(route('qasa.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         $payment = Payments::where('customer_id','=',$id)->get();
         $customers = Customers::all();
         $customer = Customers::where('id','=',$id)->get();
         $i=0;
         return view('payments.detail',['payment'=>$payment,'customers'=>$customers,'customer'=>$customer,'i'=>$i]);
    }
    public function search(Request $request)
    {
        //
       $qasa = Qasa::all();
        $xawanQasa = Payments::all();
        $i=0;
        return view('payments.payment',['qasa'=>$qasa, 'xawanQasa'=>$xawanQasa,'i'=>$i]);
            }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $payment = Payments::find($id);
        $customers = Customers::all();
        return view('payments.edit',['payment'=>$payment,'customers'=>$customers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $obj=Payments::find($id);
        $obj->amount= $request->input("amount");
        $obj->date= $request->input("date");
        $obj->note= $request->input("note");
        $obj->customer_id= $request->input("customer_id");
        $obj->save();
        return redirect(route('payments.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
          error_log('Some message here.');
        $gumrg = Payments::findOrFail($id);
        $gumrg->delete();
        \Session::flash('flash_message_delete','Payment successfully deleted.');

        return redirect(route('payments.index'));

    }
}
