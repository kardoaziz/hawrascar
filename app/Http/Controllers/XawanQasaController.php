<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\XawanQasa;
class XawanQasaController extends Controller
{
       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $i=0;
        $XawanQasa = XawanQasa::paginate(10);
        return view('xawanQasa.index',["XawanQasa"=>$XawanQasa,"i"=>$i]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function search(Request $request)
    {
        //
        $search = $request->input("search");
        $obj  = XawanQasa::where('name','like','%'.$search.'%')->paginate(10);
        $i=0;
        return view("xawanQasa.index",['XawanQasa'=>$obj,'i'=>$i]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          //
        $obj = new XawanQasa();
        $obj->name=$request->input('name');
        $obj->number=$request->input('phone');
        $obj->address=$request->input('address');
        $obj->save();
        return redirect(route('xawanQasa.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $XawanQasa = XawanQasa::find($id);
        // $XawanQasa = XawanQasa::all();
        return view('xawanQasa.edit')->with('XawanQasa',$XawanQasa);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $obj=XawanQasa::find($id);
        $obj->name= $request->input("name");
        $obj->number= $request->input("phone");
        $obj->address= $request->input("address");
        $obj->save();
        return redirect(route('xawanQasa.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $xawanQasa = XawanQasa::findOrFail($id); 
        $xawanQasa->delete();

        return redirect()->route('xawanQasa.index');
    }
}
