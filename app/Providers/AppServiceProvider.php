<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    // public function register()
    // {
    //     //
    // }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        Schema::defaultStringLength(191);

         $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            $event->menu->add('MAIN NAVIGATION');
            $event->menu->add([
                'text' => 'كڕیاره‌كان',
                'url' => 'customers',
                'icon'=>'users'
            ],[
                'text' => 'گومرگ',
                'url' => 'gumrgs',
                'icon'=>'truck'
            ],[
                'text' => 'نقل',
                'url' => 'naql',
                'icon'=>'exchange'
            ],[
                'text' => 'جۆری ئوتومبیل',
                'url' => 'cars',
                'icon'=>'car'
            ],[
                'text' => 'ڕه‌نگه‌كان',
                'url' => 'colors',
                'icon'=>'list'
            ],[
                'text' => 'پاره‌دان',
                'url' => 'payments',
                'icon'=>'money'
            ],[
                'text' => 'مه‌رز',
                'url' => 'marz',
                'icon'=>'ship'
            ],[
                'text'    => 'قاصه‌',
                'url'     => '#',
                'icon' =>'bank',
                'submenu' => [
                    [
                        'text' => 'خاوه‌ن قاصه‌كان',
                        'url'  => 'xawanQasa',
                        'icon' => 'users'
                    ],
                    [
                        'text' => 'حساباتی قاصه‌',
                        'url'  => 'qasa',
                        'icon' => 'money'
                    ],
                ],
            ]);
        });
    }
}
