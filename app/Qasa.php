<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qasa extends Model
{
    public function xawanQasa(){
        return $this->belongsTo('App\Qasa','id');
    }
}
