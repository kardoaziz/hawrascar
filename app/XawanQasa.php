<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class XawanQasa extends Model
{
    public function qasa() {
        return $this->hasMany('App\XawanQasa');
    }

}
