<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGumrgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gumrgs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->integer('car_name');
            $table->integer('color');
            $table->string('year');
            $table->string('specification');
            $table->string('vin');
            $table->string('temp_plate');
            $table->string('gumrg');
            $table->integer('balance');
            $table->integer('transfer_fee');
            $table->integer('total');
            $table->bigInteger('customer_id')->unsigned();
            $table->date('import_date');
            $table->date('export_date');
            $table->string('contract_no');
            $table->timestamps();
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->index('customer_id');

   //           Schema::table('gumrgs', function($table) {
   //     $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade')->onUpdate('cascade');
   // });

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gumrgs');
    }
}
