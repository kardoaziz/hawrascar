<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkeyToBalance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('balances', function (Blueprint $table) {
            //
            $table->integer('id_marz')->unsigned()->change();
            $table->foreign('id_marz')->references('id')->on('marzs')->onDelete('cascade');
            $table->index('id_marz');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('balances', function (Blueprint $table) {
            $table->dropColumn('id_marz');
            $table->dropForeign('balances_id_marz_foreign');
        });    }
    
}
