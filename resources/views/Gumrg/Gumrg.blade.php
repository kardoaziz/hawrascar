@extends('adminlte::page')
@section('title', 'AdminLTE')

@section('content_header')

@stop
@section('content')
  <!-- <h1> Gumrg Works</h1> -->
<style>
#wrapper2 {
    width: 80px;
    /*border: 1px solid black;*/
    overflow: hidden; /* will contain if #first is longer than #second */
}
#first {
    width: 40px;
    float:left; /* add this */
    /*border: 1px solid red;*/
}
#second {
  width: 40px;
    /*border: 1px solid green;*/
    overflow: hidden; /* if you don't want #second to wrap below #first */
}
.Row2{
 display: table;
    width: 170px; /*Optional*/
    table-layout: fixed; /*Optional*/
    border-spacing: 10px; /*Optional*/
}
.bg1{
    background-color:#DC143C;
  color:white;
}
.bg2{
  background-color:#9ACD32;
  color:white;
}
.bg3{background-color:#F4A460;

  color:white;
}
.bg4{
  background-color:#20B2AA;
  color:white;
}
.greybg{
  background-color:#DCDCDC;
  color:black;
}

.Column{
  display: table-cell;
}
</style>
<div class="box" style="font-family:'Ali-K-Samik-akurdnet';">

            <div class="box-header" style="text-align: center">

              <div class="col-sm-12" style="font-family: Ali-K-Samik"><h1>به‌شی گومرگ</h1></div>
              <div class="container">
                <div class="row">
                <div class="col-md-3 pull-right"><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#insert-modal">
                    <i class="fa fa-edit">زیادكردن</i>
                  </button></div>
                <div class="col-md-3 pull-left"><form method="get" action="{{ route('gumrgs.search') }}">
                  <div class="input-group input-group-sm" >
                    
                    <input type="text" name="search" class="form-control pull-right" placeholder="Search">
  
                    <div class="input-group-btn">
                      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                  </form></div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <form class="form-horizontal" method="get" action="{{ route('gumrgs.search_customer') }}">
                            <div class="input-group">
                                <div class="input-group-btn">
                                  <input type="submit" class="btn btn-danger" value="گه‌ڕان به‌ پێی ناوی كڕیار">
                                </div>
                                <!-- /btn-group -->
                                <select class="form-control" style="width: 40%; direction: rtl" name="search">
                                    @foreach ($customers as $c)
                                    <option value="{{ $c->id }}" {{ ($selectedC == $c->id) ? 'selected' : '' }}>{{ $c->name }}</option>
                                    @endforeach
                                  </select>
                                  @if($fdate)
                                  <input style="width:22%;" type="date" name="from_date" value="{{$fdate}}">
                                  @else 
                                  <input style="width:22%;" type="date" name="from_date" >
                                  @endif
                                   @if($tdate)
                                  <input style="width:22%;" type="date" name="till_date" value="{{$tdate}}">
                                  @else 
                                  <input style="width:22%;" type="date" name="till_date" >
                                  @endif
                              </div>
                              <div class="row>">


                              </div>

                        </form>
                      </div>
                </div>
                </div>
            </div>


              <div class="table-responsive">

                <table  class="table table-hover text-nowrap table-bordered">
                <div class="row">
                </div>
                <thead>

                <tr scope="row">
                	<th >ژ.عقد</th>
                	<th >به‌رواری ده‌رچوون</th>
                	<th >به‌رواری هاتن</th>
                	<th class="bg1">كۆی گشتی</th>
                	<th class="bg2">نقل</th>
                	<th class="bg3">رصید</th>
                	<th class="bg4">گومرگ</th>
                	<th >ژماره‌ی كاتی</th>
                	<th >ژماره‌ی شانصی</th>
                	<th >تایبه‌تمه‌ندی</th>
                  <th >بستۆن</th>
                	<th >ساڵ</th>
                	<th >ڕه‌نگ</th>
                	<th >جۆری ئوتومبیل</th>
                	<th >ناوی كڕیار</th>
                  <th >مه‌رز</th>
                	<th >#</th>
                	<th >Edit</th>
                	
                </tr>
                </thead>
                <tbody>
                	@foreach($gumrg as $c)
                <tr scope="row" class="{{ ($c->export_date) ? 'greybg' : '' }}">
	                  <td>{{$c->contract_no}}</td>
	                  <td>{{$c->export_date}}</td>
	                  <td>{{$c->import_date}}</td>
	                  <td class="bg1">{{$c->total}}</td>
	                  <td class="bg2">{{$c->transfer_fee}}</td>
	                  <td class="bg3">{{$c->balance}}</td>
	                  <td class="bg4">{{$c->gumrg}}</td>
	                  <td>{{$c->temp_plate}}</td>
	                  <td>{{$c->vin}}</td>
	                  <td style="direction: rtl;">{{$c->specification}}</td>
	                  <td>{{$c->cylinder}}</td>
                    <td>{{$c->year}}</td>
	                  <td>
                      @foreach ($colors as $co)
                          @if ($co->id==$c->color)
                              {{ $co->name }}
                          @endif
                      @endforeach
                    </td>
	                  <td>@foreach ($cars as $ca)
                        @if ($ca->id==$c->car_name)
                        {{$ca->name}}-{{$ca->company}}
                        @endif
                    @endforeach</td>
	                  <td>{{$c->customers->name}}</td>
                    <td>{{$c->marz->name}}</td>
	                  <td>{{++$i}}</td>
	                  <td>
                      <div class="Row2">
                            <div class="Column"><form method="GET"action="{{ route('gumrgs.edit',$c->id) }}">
                            {{ csrf_field() }}
          
                            <div class="form-group">
                                <input type="submit" class="btn btn-warning btn-xs " value="نوێكردنه‌وه‌" >
                            </div>
                          </form>
                        </div>
                        <div class="Column"><form method="post" action="{{ route('gumrgs.destroy',$c->id) }}" >
                                      {{ csrf_field() }} 
                                      <input type="hidden" name="_method" value="DELETE"  >
                                      <button class="btn btn-danger btn-xs" onclick="return confirm('دڵنیایت له‌ سڕینه‌وه‌؟')">سڕینه‌وه‌</button>
                          </form>
                        </div>
                        <div class="Column"><form method="get" action="{{ route('gumrgs.show',$c->id) }}" >
                                      {{ csrf_field() }} 
                                      <div class="form-group">
                                      <input type="submit" class="btn btn-success btn-xs  " value="عقد" >
                    
                                  </div>
                        </form>
                      </div>
                    </div>
                      
                      
                      
	                  </td>
	                </tr>
	                @endforeach
	            </tbody>
                  
              <tfoot>
                <tr>
                  <td></td>
                  <td></td>
                  
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
              </tfoot>
              </table>

            <div class="text-center">
            {{ $gumrg->links() }}
            </div>
            </div>
          </div>
            </div>

            </div>
            <div class="modal fade" id="insert-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" align="center"><b>New Gumrg</b></h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('gumrgs.store') }}" method="post">
                {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="box-body">
            <table style="direction: rtl" class="table table-striped table-responsive">
            <tbody>
            <tr>
                <td style="width:30%;"> <label for="exampleInputEmail1">مه‌رز</label> </td>
              <td><select style="width:100%" class="form-control" name="id_marz">
                    @foreach($marz as $mz)
                      <option value="{{$mz->id}}">{{$mz->name}} -{{ $mz->balance - $mz->total}}</option>
                  @endforeach
                  </select></td>
                  
                </tr>
                <tr>
                <td style="width:30%;"> <label for="exampleInputEmail1">ناوی كڕیار</label> </td>
              <td><select style="width:100%" class="form-control" name="customer_id">
                    @foreach($customers as $c)
                      <option value="{{$c->id}}">{{$c->name}}</option>
                  @endforeach
                  </select></td>
                  
                </tr>
            <tr>
              <td><button type="button" class="btn btn-xs" data-toggle="modal" data-target="#new-car-modal"><i class="fa fa-fw fa-plus-circle"></i></button>
                <label for="exampleInputEmail1">جۆری ئوتومبیل</label></td> 
                  <td><select style="width:100%" class="form-control" name="car_name">
                    @foreach($cars as $ca)
                      <option value="{{$ca->id}}">{{$ca->name}} - {{$ca->company}}</option>
                  @endforeach
                  </select></td>
            </tr>
            <tr><td><button type="button" class="btn btn-xs" data-toggle="modal" data-target="#new-color-modal"><i class="fa fa-fw fa-plus-circle"></i></button>
              <label for="exampleInputEmail1">ڕه‌نگ</label></td>  
                  <td><select style="width: 100%" class="form-control col-sm-8" name="color">
                    @foreach($colors as $co)
                      <option value="{{$co->id}}">{{$co->name}}</option>    
                  @endforeach
                  </select></td>
                 
            </tr>
            <tr><td>
              <label for="exampleInputEmail1">ساڵ</label></td> 
              <td><input style="width: 100%" type="text" name="year" class="form-control col-sm-9" placeholder="Year"></td>
            </tr>
            <tr><td>
              <label for="exampleInputEmail1">ژماره‌ی شانصی</label></td> 
              <td><input style="width: 100%" type="text" name="vin" class="form-control col-sm-9"  placeholder="VIN"></td>
            </tr>
            <tr><td>
              <label for="exampleInputEmail1">بستۆن </label></td> 
              <td><input style="width: 100%" type="text" name="cylinder" class="form-control col-sm-9"  placeholder="cylinder"></td>
            </tr>
            <tr><td>
              <label for="exampleInputEmail1">تایبه‌تمه‌ندی</label></td> 
              <td><input style="width: 100%" type="text" name="specification" class="form-control col-sm-9"  placeholder="Specification"></td>
            </tr>
            <tr><td>
              <label for="exampleInputEmail1">ژماره‌ی كاتی</label></td> 
              <td><input style="width: 100%" type="text" name="temp_plate" class="form-control col-sm-9"  placeholder="Temp Plate"></td>
            </tr>
            <tr><td>
              <label for="exampleInputEmail1">گومرگ</label></td> 
              <td><input style="width: 100%" type="number" name="gumrg" id="gumrg" class="form-control col-sm-9" placeholder="Custom"></td>
            </tr>
            <tr><td>
              <label for="exampleInputEmail1">رصید</label></td> 
              <td><input style="width: 100%" type="number" name="balance" id="balance" class="form-control col-sm-9"  placeholder="Balance"></td>
            </tr>
            <tr><td>
              <label for="exampleInputEmail1">نقڵ</label></td> 
              <td><input style="width: 100%" type="number" name="transfer_fee" id="transfer_fee" class="form-control col-sm-9"  placeholder="Transfer fee"></td>
            </tr>
            <tr><td>
              <label for="exampleInputEmail1">كۆی گشتی</label></td> 
              <td><input style="width: 100%" type="text" id="total" name="total" class="form-control col-sm-9"  placeholder="Total" readonly></td>
            </tr>
            <tr><td>
              <label for="exampleInputEmail1">به‌رواری هاتن</label></td> 
              <td><input style="width: 100%" type="date" name="import_date" class="form-control col-sm-9" placeholder="Import Date" ></td>
            </tr>
            <tr><td>
              <label for="exampleInputEmail1">به‌رواری ده‌رچوون</label></td> 
              <td><input style="width: 100%" type="date" name="export_date" class="form-control col-sm-9"  placeholder="Export Date"></td>
            </tr>
            <tr><td>
              <label for="exampleInputEmail1">ژماره‌ی عقد</label></td> 
              <td><input style="width: 100%" type="text" name="contract_no" class="form-control col-sm-9"  placeholder="Contract no."></td>
            </tr>
            <tr><td colspan="2"><button style="width: 100%" type="submit" class="btn btn-success">زیادكردن</button></td></tr> 
          </div>
        </tbody>
        </table>
        </form>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="new-color-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" align="center"><b>زیادكردنی ڕه‌نگ</b></h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('colors.store') }}" method="post">
                {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">ناوی ڕه‌نگ</label> 
              <input type="text" name="new_color" class="form-control" placeholder="New Color">
            </div>
          </div>
          <button type="submit" class="btn btn-primary">زیادكردن</button>
        </form>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">داخستن</button>
            
          </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="new-car-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" align="center"><b>زیادكردنی جۆری ئوتومبێل</b></h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('cars.store') }}" method="post">
                {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="box-body">
             <div class="form-group">
              <label for="exampleInputEmail1">ناوی كۆمپانیا</label> 
              <input type="text" name="company" class="form-control" placeholder="Company">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">ناوی سه‌یاره‌</label> 
              <input type="text" name="name" class="form-control" placeholder="Name">
            </div>
          </div>
          <button type="submit" class="btn btn-primary">زیادكردن</button>
        </form>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">داخستن</button>
            
          </div>
      </div>
    </div>
  </div>
</div>
            <!-- /.box-body -->
          </div>
  @stop
  @section('adminlte_js')
  <script type="text/javascript">
     $(document).ready(function(){
    $(function(){
              $('#gumrg, #transfer_fee, #balance').keyup(function(){
                 var value1 = parseFloat($('#gumrg').val()) || 0;
                 var value2 = parseFloat($('#transfer_fee').val()) || 0;
                 var value3 = parseFloat($('#balance').val()) || 0;
                 $('#total').val(value1 + value2 + value3);
              });
           });
  });
  </script>
  @stop