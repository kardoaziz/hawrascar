@extends('adminlte::page')
@section('title', 'AdminLTE')

@section('content_header')
    <!-- <h1>Customer View</h1> -->
@stop
@section('content')

<div class="box box-primary" style="font-size:20px;">
            <div class="box-header with-border">
              <h3 class="box-title">عقد شركه‌</h3>
              <h2 >{{$gumrg->car->company }} {{$gumrg->car->name }}</h2>
              <h2 >  {{ $gumrg->vin }} لاشه‌</h2>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <!--  {{$contract}} -->
            @if(sizeof($contract)>0)
            
         	<form style="" action="{{ route('gumrgs.insertContract') }}" method="get">
                {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
         <input type="hidden" class="form-control" name="id_gumrg" placeholder="Export Date" value="{{ $gumrg->id }}">
              <div class="box-body">
                <div class="form-group">
                  <label >به‌رواری ده‌رچوون</label>
                  <input type="date" class="form-control" name="date" placeholder="Export Date" value="{{ $contract->date }}">
                </div>
                <div class="form-group">
                  <label >ناوی كڕیار</label>
                  <input type="text" class="form-control" name="buyer"  placeholder="Enter Name" value="{{ $contract->buyer }}">
                </div>
                <div class="form-group">
                  <label >ژ.ته‌له‌فۆنی كڕیار</label>
                  <input type="text" class="form-control" name="phone"  placeholder="Phone" value="{{ $contract->phone }}">
                </div>
                <div class="form-group">
                  <label >ناونیشان</label>
                  <input type="text" placeholder="Address" name="address" class="form-control" value="{{ $contract->address }}">
                </div>
                <div class="form-group">
                  <label >ژ.نوسراووی گومرگ</label>
                  <input type="text" placeholder="Attach" name="attach_no" class="form-control" value="{{ $contract->attach_no }}">
                </div>
                <div class="form-group">
                  <label >به‌رواری ده‌رچوونی نوسراوی گومرگ</label>
                  <input type="date" placeholder="attach date" name="attach_date" class="form-control" value="{{ $contract->attach_date }}">
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
						              </div>
            </form>
            <!-- {{$contract}} -->
             <form method="get" action="{{ route('gumrgs.showContract') }}" >
                        {{ csrf_field() }}
       					<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
       					<input type="hidden" name="id_gumrg" value="{{$gumrg->id}}"  >
                        <div class="form-group">
                            <input type="submit" class="btn btn-success btn-lg " value="پرنت" >
                        </div>
                </form>
            @else
            <form style="" action="{{ route('gumrgs.insertContract') }}" method="get">
                {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
         <input type="hidden" class="form-control" name="id_gumrg" placeholder="Export Date" value="{{$gumrg->id}}">
              <div class="box-body">
                <div class="form-group">
                  <label >به‌رواری ده‌رچوون</label>
                  <input type="date" class="form-control" name="date" placeholder="Export Date">
                </div>
                <div class="form-group">
                  <label >ناوی كڕیار</label>
                  <input type="text" class="form-control" name="buyer"  placeholder="Enter Name">
                </div>
                <div class="form-group">
                  <label >ژ.ته‌له‌فۆنی كڕیار</label>
                  <input type="text" class="form-control" name="phone"  placeholder="Phone">
                </div>
                <div class="form-group">
                  <label >ناونیشان</label>
                  <input type="text" placeholder="Address" name="address" class="form-control">
                </div>
                <div class="form-group">
                  <label >ژ.نوسراووی گومرگ</label>
                  <input type="text" placeholder="Attach" name="attach_no" class="form-control">
                </div>
                <div class="form-group">
                  <label >به‌رواری ده‌رچوونی نوسراوی گومرگ</label>
                  <input type="date" placeholder="attach date" name="attach_date" class="form-control">
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
               <!--  <button type="submit" class="btn btn-success">print</button> -->
              </div>
            </form>
            @endif
          </div>

@stop