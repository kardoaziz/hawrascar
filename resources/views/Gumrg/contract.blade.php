
<style type="text/css">
        

body {
	 font-family: "Unikurd Jino";
	 width: 21cm;
  height: 29.5cm; 
  margin-left:auto;
  margin-right:auto;
  padding-left:1cm;
  padding-right:1cm;
  background: rgb(204,204,204); 
  -webkit-print-color-adjust: exact !important;
	}
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);

}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
  padding-bottom: 50px;
}
.logo{
	height:150px;
	width:200px;
}
.text-center{
	text-align: center;
}
.greybg{
	background-color:#C1D8CF;
	border:0px;
}
.textr{
	text-align: right;
}
.textl{
	text-align: left;
}
.textc{
	text-align: center;
}


table tr td {
	direction :rtl;
}

@media print
{     body.receipt { width: 58mm }
    .noprint, .noprint *
    {
        display: none !important;
        height:0px;
    }
    page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0cm rgba(0,0,0,0);

}
}

</style>
<body onload="window.print()">
<page size="A4" >
	<button onclick="window.history.back();" class="noprint" style="background-color:red;width:100px;height:50px;border-radius:30px;font-weight:bold;font-size:17px;">Close</button>

<table style="width:100%;height:100%;margin-bottom:50px;" >
	<!-- {{$gumrg}} -->
	<tr style="text-align: center;line-height: 1.8;">
		<td style="font-size: 28px;  color:#FAE101;font-family:'TraditionalArabic';">
			شركة 
				<br> هوراس عباس محمود 
				<br> لتجاره‌ السیارات / مفرد 
				<br>
				<div style="font-size: 12px;color:black"> سلیمانیة شارع ٦٠م- قرب محطة بیشةسازی</div>
			
		</td>
		<td colspan="2">
			<img class="logo" src="{{ asset('imgs/logo.jpg') }}">
		</td>
		<td style="font-size: 28px;color:#FAE101;">كۆمپانیای 
			<br> هوراس عباس محمود 
			<br>  بۆ بازرگانی ئوتومبێل / تاك 
			<br>
			<div style="font-size: 12px;color:black;font-family:'Unikurd Jino';"> سلێمانی شه‌قامی ٦٠ م-نزیك به‌نزیخانه‌ی پیشه‌سازی</div>
		</td>

	</tr>
	<tr>
		<td colspan="4" class="textc" style="font-size: 20px;color:#DB485C;padding-top:20px;font-weight: bold;">ژماره‌ی مۆبایل / 07702464486</td>
	</tr>
	<tr class="greybg" style="font-size: 18px;">
		<td  class="textr" style="padding-right:15px;padding:5px;"> به‌روار / {{$contract->date}} </td>
		<td colspan="2" class=""></td>
		<td  class="textr" style="padding-right:15px;"> ژماره‌ / {{$contract->id}}</td>
	</tr>
	<tr style="">
		<td colspan="4" class="textc" style="font-size: 20px;font-weight: bold;padding-top:30px;" >بۆ / به‌ڕێوه‌به‌رایه‌تی هاتووچۆی هه‌ولێر - سلێمانی - دهۆك</td>
	</tr>
	<tr>
		<td colspan="4" class="textc" style="font-size: 20px;font-weight: bold;padding-top:8px;">بابه‌ت / گرێبه‌ستی فرۆشتن (عقد)</td>
	</tr>
	<tr style="font-size: 18px;height:35px;" class="textr">
		<td colspan="2" class="textr" style=""> ناوی لایه‌نی دووه‌م / <a style="font-weight:bold;">{{$contract->buyer}}</a></td>
		<td class="textr" colspan="2" style=""> ناوی لایه‌نی یه‌كه‌م / <a style="font-weight:bold;"> هوراس عباس محمود</a>
		 </td>
	</tr>
	<tr style="font-size: 18px;">
		<td class="textr" colspan="2" style="">ژماره‌ی موبایل / {{$contract->phone}}</td>
		<td colspan="2" class="textr" style="">ژماره‌ی موبایل / 07702464486 </td>
	</tr>
	<tr style="font-size: 18px;">
		<td colspan="2" class="textr" style="">ناونیشان / {{$contract->address}}</td>
		<td colspan="2" class="textr" style="">ناونیشان/ سلێمانی شه‌قای ٦٠ متری </td>
	</tr>
	<tr style="font-size: 20px;height:35px;">
		<td colspan="4" class="text-center greybg">ژماره‌ی ﴿ {{$gumrg->temp_plate}} ﴾ كاتی </td>
	</tr>
	<tr style="font-size: 20px;height:35px;">
		<td colspan="2" class="textr" style="height:35px;">مۆدێل / {{$gumrg->year}}</td>
		<td colspan="2" class="textr" style="height:35px;">جۆری ئوتومبێل / {{$gumrg->car->company}} {{$gumrg->car->name}}</td>
		
	</tr>
	<tr style="font-size: 20px;height:35px;">
		<td colspan="2" class="textr" style="">بستۆن / {{$gumrg->cylinder}} </td>
		<td colspan="2" class="textr" style="text-transform: capitalize;">لاشه‌ / {{$gumrg->vin}}</td>
	</tr>
	<tr style="font-size: 20px;height:35px;">
		<td colspan="2" class="textr" style="">ره‌نگ / {{$gumrg->colored->name}}</td>
		<td colspan="2" class="textr"></td>
	</tr>
	<tr style="font-size: 18px;">
		<td colspan="2" class="textr" style="">كات / 9:00 به‌یانی</td>
		<td colspan="2" class="textr" style="">ڕێكه‌وتی ده‌رچوونی له‌ كۆمپانیا / {{ $contract->date }}</td>
	</tr>
	<tr style="font-size: 18px;padding:10px;">
		<td colspan="4" class="textr greybg" style="padding:5px;color:#DB485C;line-height: 1.6;font-weight: bold;">تێبینی: ئه‌بێت له‌ دوای واژۆ كردنی ئه‌م گرێبه‌سته‌ سه‌ردانی به‌رێوه‌به‌رایه‌تی هاتووچۆ بكرێت به‌ مه‌به‌ستی تۆماركردنی , ئه‌گه‌ر له‌ ماوه‌ ٦٠ رۆژ سالیانه‌ و تابلۆ وه‌رنه‌گرێت سزا ده‌درێت به‌ بڕی (1000000) دینار</td>
	</tr>
	<tr style="font-size: 20px;">
		<td colspan="4" class="" style="padding:7px;line-height: 1.8;">تكایه‌ ڕه‌زامه‌ندی بفه‌رموون له‌سه‌ر تۆماركردنی ئه‌م ئوتومبێله‌ كه‌ خه‌سله‌ته‌كانی دیاری كراوه‌ به‌ ناوی ( {{$contract->buyer}} ) به‌پێی گرێبه‌ستی كۆمپانیا ژماره‌ ( {{$contract->id}} ) له‌ ڕێكه‌وتی ( {{$contract->date}} ) نوسراوی به‌ڕێوه‌به‌رایه‌تی گومرگی ({{$gumrg->marz->name}}) ژماره‌ ( {{$contract->attach_no}} ) له‌ ڕێكه‌وتی ( {{$contract->attach_date}} )</td>
	</tr>
	<tr style="height:100px;">
	</tr>
	<tfoot>
	<tr style="font-size: 18px;" class="textc">
		<td  class="" style="font-weight: bold;font-size: 22px;">{{$contract->buyer}}</td>
		<td colspan="2" class=""></td>
		<td  class="" style="font-weight: bold;font-size: 22px;">هوراس عباس محمود</td>
	</tr>
	<tr style="font-size: 18px;" class="textc">
		<td  class="textc">به‌ڵێن و ره‌زامه‌ندی لایه‌نی دووه‌م</td>
		<td colspan="2" class="textc">نوسراوی كۆمپانیا</td>
		<td  class="textc">به‌ڵێن و ره‌زامه‌ندی لایه‌نی دووه‌م</td>
	</tr>
	<tr style="font-size: 18px;" class="textc">
		<td  class="textc">واژۆی كریار</td>
		<td colspan="2" class="textc">مۆو و واژوو</td>
		<td  class="textc">واژۆی فرۆشیار</td>
	</tr>
</tfoot>
</table>
</page>
</body>
