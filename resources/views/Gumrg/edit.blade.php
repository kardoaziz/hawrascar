@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <!-- <h1>Customer View</h1> -->
@stop
@section('content')
  <!-- <h1> </h1> -->
<!-- {{$marz}} -->
<div class="box">
            <div style="text-align: center" class="box-header">
              <h3 class="box-title">نوێكردنه‌وه‌ی گۆمرگ</h3>
            </div>
            <!-- /.box-header -->
            <!-- /////////// -->
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
              	<div class="row">
                  <div class="col-sm-3"></div>
              		<div class="col-sm-6">

          <form style="" action="{{ route('gumrgs.update',$gumrg->id) }}" method="post" oninput="total.value = parseInt(gumrg.value) + parseInt(transfer_fee.value)+parseInt(balance.value)">
              {{csrf_field()}}
                  <input type="hidden" name="_method" value="PUT">

        <div class="box-body">
          <table style="direction: rtl" class="table table-striped table-responsive">
          <tbody>
          <tr>
              <td style="width:30%;"> <label for="exampleInputEmail1">مه‌رز</label> </td>
            <td><select style="width:100%" class="form-control" name="id_marz">
                  @foreach($marz as $mz)
                    
                    @if ($mz->id == $gumrg->id_marz)
                    <option value="{{$mz->id}}" selected>{{$mz->name}} - {{ $mz->balance - $mz->total}}</option>
                    @else 
                    <option value="{{$mz->id}}">{{$mz->name}} -{{ $mz->balance - $mz->total}}</option>
                    @endif
                @endforeach
                </select></td>
                
              </tr>
              <tr>
              <td style="width:30%;"> <label for="exampleInputEmail1">ناوی كڕیار</label> </td>
            <td><select style="width:100%" class="form-control" name="customer_id">
                  @foreach($customers as $c)
                    
                    @if ($c->id == $gumrg->customer_id)
                    <option value="{{$c->id}}" selected>{{$c->name}}</option>
                    @else 
                    <option value="{{$c->id}}">{{$c->name}}</option>
                    @endif
                @endforeach
                </select></td>
                
              </tr>
          <tr>
            <td><label for="exampleInputEmail1">گۆری ئوتومبیل</label></td> 
                <td><select style="width:100%" class="form-control" name="car_name">
                  @foreach($cars as $ca)
                    @if ($ca->id==$gumrg->car_name)
                    <option value="{{$ca->id}}" selected>{{$ca->name}} - {{$ca->company}}</option>
                    @else 
                    <option value="{{$ca->id}}">{{$ca->name}} - {{$ca->company}}</option>
                    @endif
                @endforeach
                </select></td>
          </tr>
          <tr><td>
            <label for="exampleInputEmail1">ڕه‌نگ</label></td>  
                <td><select style="width: 100%" class="form-control col-sm-8" name="color">
                  @foreach($colors as $co)
                    @if ($co->id==$gumrg->color)
                    <option value="{{$co->id}}" selected>{{$co->name}}</option>
                    @else
                    <option value="{{$co->id}}">{{$co->name}}</option>
                    @endif
                    
                @endforeach
                </select></td>
               
          </tr>
          <tr><td>
            <label for="exampleInputEmail1">ساڵ</label></td> 
            <td><input style="width: 100%" type="text" name="year" class="form-control col-sm-9" value="{{ $gumrg->year }}" placeholder="Year"></td>
          </tr>
          <tr><td>
            <label for="exampleInputEmail1">ژماره‌ی شانصی</label></td> 
            <td><input style="width: 100%" type="text" name="vin" class="form-control col-sm-9" value="{{ $gumrg->vin }}" placeholder="VIN"></td>
          </tr>
          <tr><td>
            <label for="exampleInputEmail1">تایبه‌تمه‌ندی</label></td> 
            <td><input style="width: 100%" type="text" name="specification" class="form-control col-sm-9" value="{{ $gumrg->specification }}" placeholder="Specification"></td>
          </tr>
          <tr><td>
            <label for="exampleInputEmail1">بستۆن</label></td> 
            <td><input style="width: 100%" type="text" name="cylinder" class="form-control col-sm-9" value="{{ $gumrg->cylinder }}" placeholder="Cylinder"></td>
          </tr>
          <tr><td>
            <label for="exampleInputEmail1">ژماره‌ی كاتی</label></td> 
            <td><input style="width: 100%" type="text" name="temp_plate" class="form-control col-sm-9" value="{{ $gumrg->temp_plate }}" placeholder="Temp Plate"></td>
          </tr>
          <tr><td>
            <label for="exampleInputEmail1">گومرگ</label></td> 
            <td><input style="width: 100%" type="number" name="gumrg" id="gumrg" class="form-control col-sm-9" value="{{ $gumrg->gumrg }}" placeholder="Custom"></td>
          </tr>
          <tr><td>
            <label for="exampleInputEmail1">رصید</label></td> 
            <td><input style="width: 100%" type="number" name="balance" id="balance" class="form-control col-sm-9" value="{{ $gumrg->balance }}" placeholder="Balance"></td>
          </tr>
          <tr><td>
            <label for="exampleInputEmail1">نقڵ</label></td> 
            <td><input style="width: 100%" type="number" name="transfer_fee" id="transfer_fee" class="form-control col-sm-9" value="{{ $gumrg->transfer_fee }}" placeholder="Transfer fee"></td>
          </tr>
          <tr><td>
            <label for="exampleInputEmail1">كۆی گشتی</label></td> 
            <td><input style="width: 100%" type="text" id="total" name="total" class="form-control col-sm-9" value="{{ $gumrg->total }}" placeholder="Total" readonly></td>
          </tr>
          <tr><td>
            <label for="exampleInputEmail1">به‌رواری هاتن</label></td> 
            <td><input style="width: 100%" type="date" name="import_date" class="form-control col-sm-9" value="{{ $gumrg->import_date }}" placeholder="Import Date" ></td>
          </tr>
          <tr><td>
            <label for="exampleInputEmail1">به‌رواری ده‌رچوون</label></td> 
            <td><input style="width: 100%" type="date" name="export_date" class="form-control col-sm-9" value="{{ $gumrg->export_date }}" placeholder="Export Date"></td>
          </tr>
          <tr><td>
            <label for="exampleInputEmail1">ژماره‌ی عقد</label></td> 
            <td><input style="width: 100%" type="text" name="contract_no" class="form-control col-sm-9" value="{{ $gumrg->contract_no }}" placeholder="Contract no."></td>
          </tr>
          <tr><td colspan="2"><button style="width: 100%" type="submit" class="btn btn-warning">نوێكردنه‌وه‌</button></td></tr> 
        </div>
      </tbody>
      </table>
      </form>
            </div>
        	</div>
    		</div>
			</div>
           </div>
            <!-- /.box-body -->
          </div>
  @stop