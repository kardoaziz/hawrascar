@extends('adminlte::page')

@section('title', 'Hawras-Cars')

@section('content_header')
    <!-- <h1>Customer View</h1> -->
@stop

@section('content')
  
<div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">New Customer</h3>
            </div>
            <form action="{{ route('cars.update',$car->id) }}" method="post">
          {{csrf_field()}}
          <input type="hidden" name="_method" value="PUT">
            <div class="box-body">
              <div class="row">
                <div class="col-xs-3">
                        <input type="text" name="company" class="form-control" value="{{ $car->company }}">
                        </div>
                <div class="col-xs-2">
                  <input type="text" name="name" class="form-control" value="{{ $car->name }}">
                </div>
                <div class="col-xs-2">
                  <button type="submit" class="btn btn-block btn-lg btn-warning btn-lg">نوێكردنه‌وه‌</button>
                </div>
              </div>
            </div>
        </form>
            <!-- /.box-body -->
          </div>
            <!-- /.box-body -->
          </div>
@stop