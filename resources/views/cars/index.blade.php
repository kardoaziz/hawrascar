@extends('adminlte::page')

@section('title', 'Hawras-Cars')

@section('content_header')
    <!-- <h1>Customer View</h1> -->
@stop

@section('content')
  
<div class="box">
            <div class="box-header">
                <h1 style="text-align: center">به‌شی ئوتومبیله‌كان</h1>

            </div>
            <!-- /.box-header -->
            <div class="box box-danger">
            <div class="box-header with-border">
              
            </div>
            <form action="{{ route('cars.store') }}" method="post">
      	{{csrf_field()}}
            <div class="box-body">
              <div class="row">
                <div class="col-xs-3">
                        <input type="text" name="company" class="form-control" placeholder="كۆمپانیا">
                        </div>
                <div class="col-xs-2">
                  <input type="text" name="name" class="form-control" placeholder="ناو">
                </div>
                <div class="col-xs-2">
                  <button type="submit" class="btn btn-block btn-lg btn-success btn-lg">زیادكردن</button>
                </div>
              </div>
            </div>
        </form>
            <!-- /.box-body -->
          </div>
            <div class="box-body table-responsive no-padding table-bordered">
              <table class="table table-hover">
                <tbody><tr>
                  <th>ID</th>
                  <th>ناو </th>
                  <th>كۆمپانیا</th>
                  <th>ده‌ستكاری كردن</th>
                  <th>سڕینه‌وه‌</th>
                </tr>
                 @foreach($cars as $c)
				    <tr>
				      <th scope="row">{{++$i}}</th>
				      <td>{{$c->name}}</td>
				      <td>{{$c->company}}</td>
				      <td><form method="GET"action="{{ route('cars.edit',$c->id) }}">
					        {{ csrf_field() }}

					        <div class="form-group">
					            <input type="submit" class="btn btn-warning btn-sm " value="Edit" >
		
					        </div>
                        </form></td>
                        <td><form method="post" action="{{ route('cars.destroy',$c->id) }}" >
                                {{ csrf_field() }} 
                                <input type="hidden" name="_method" value="DELETE"  >
                                <button class="btn btn-danger" onclick="return confirm('دڵنیایت له‌ سڕینه‌وه‌؟')">سڕینه‌وه‌</button>
                            </form></td>
				      </tr>
				      @endforeach

              </tbody></table>
            </div>
            <div class="text-center">
            {{ $cars->appends(Request::all())->render()}}
            </div>
            <!-- /.box-body -->
          </div>
@stop