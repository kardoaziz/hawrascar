@extends('adminlte::page')

@section('title', 'Hawras-Cars')

@section('content_header')
    <!-- <h1>Customer View</h1> -->
@stop

@section('content')
  
<div class="box">
            <div class="box-header">
              <h3 style="text-align:center;">كریاره‌كان</h3>

              <div class="box-tools">
                <form method="get" action="{{ route('customers.search') }}">
                <div class="input-group input-group-sm" style="width: 150px;">
                  
                  <input type="text" name="search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box box-danger">
            <div class="box-header with-border">
            </div>
            <form action="{{ route('customers.store') }}" method="post">
      	{{csrf_field()}}
            <div class="box-body">
              <div class="row">
                <div class="col-xs-2">
                  <input type="text" name="name" class="form-control" placeholder="ناو">
                </div>
                <div class="col-xs-3">
                  <input type="text" name="phone" class="form-control" placeholder="ژ.ته‌له‌فۆن">
                </div>
                <div class="col-xs-4">
                  <input type="text" name="address" class="form-control" placeholder="ناونیشان">
                </div>
                <div class="col-xs-2">
                  <button type="submit" class="btn btn-block btn-lg btn-success btn-lg">زیادكردن</button>
                </div>
              </div>
            </div>
        </form>
            <!-- /.box-body -->
          </div>
            <div class="box-body table-responsive no-padding table-bordered">
              <table class="table table-hover">
                <tbody><tr>
                  <th>#</th>
                  <th>ناو </th>
                  <th>ژماره‌ی مۆبایل</th>
                  <th>ناونیشان</th>
                  <th>Edit</th>
                </tr>
                 @foreach($customers as $c)
				    <tr>
				      <th scope="row">{{++$i}}</th>
				      <td>{{$c->name}}</td>
				      <td>{{$c->number}}</td>
				      <td>{{$c->address}}</td>
				      <td><form method="GET"action="{{ route('customers.edit',$c->id) }}">
					        {{ csrf_field() }}

					        <div class="form-group">
					            <input type="submit" class="btn btn-warning btn-sm " value="Edit" >
		
					        </div>
					    </form></td>
				      </tr>
				      @endforeach

              </tbody></table>
            </div>
            <div class="text-center">
            {{ $customers->appends(Request::all())->render()}}
            </div>
            <!-- /.box-body -->
          </div>
@stop