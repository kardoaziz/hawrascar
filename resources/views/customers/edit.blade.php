@extends('adminlte::page')

@section('title', 'Hawras-Cars')

@section('content_header')
    <!-- <h1>Customer View</h1> -->
@stop

@section('content')
  
<div class="box">
            <div class="box-header">
              <h3 class="box-title">Customers</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Customer</h3>
            </div>
            <form action="{{ route('customers.update',$customer->id) }}" method="POST">
      	{{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
            <div class="box-body">
              <div class="row">
                <div class="col-xs-2">
                  <input type="text" name="name" class="form-control" placeholder="ناو" value="{{$customer->name}}">
                </div>
                <div class="col-xs-3">
                  <input type="text" name="phone" class="form-control" placeholder="ژ.ته‌له‌فۆن" value="{{$customer->number}}">
                </div>
                <div class="col-xs-4">
                  <input type="text" name="address" class="form-control" placeholder="ناونیشان" value="{{$customer->address}}">
                </div>
                <div class="col-xs-2">
                  <button type="submit" class="btn btn-block btn-lg btn-success btn-lg">گۆرین</button>
                </div>
              </div>
            </div>
        </form>
            <!-- /.box-body -->
          </div>
           
            <!-- /.box-body -->
          </div>
@stop