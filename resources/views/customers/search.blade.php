@extends('adminlte::page')

@section('title', 'Hawras-Cars')

@section('content_header')
    <!-- <h1>Customer View</h1> -->
@stop

@section('content')
  
<div class="box">
            <div class="box-header">
              <h3 class="box-title">Customers {{ $customers->count() }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">New Customer</h3>
            </div>
            <!-- /.box-body -->
          </div>
            <div class="box-body table-responsive no-padding table-bordered">
              <table class="table table-hover">
                <tbody><tr>
                  <th>ID</th>
                  <th>Name </th>
                  <th>Phone Number</th>
                  <th>Address</th>
                  <th>Edit</th>
                  <!-- <th>Reason</th> -->
                </tr>
                @if ($customers->count() > 0)
                @foreach($customers as $c)
                <tr>
                  <th scope="row">{{++$i}}</th>
                  <td>{{$c->name}}</td>
                  <td>{{$c->number}}</td>
                  <td>{{$c->address}}</td>
                  <td><form method="GET"action="{{ route('customers.edit',$c->id) }}">
                      {{ csrf_field() }}
    
                      <div class="form-group">
                          <input type="submit" class="btn btn-warning btn-sm " value="Edit" >
        
                      </div>
                  </form></td>
                  </tr>
                  @endforeach
                @else
                  <tr><td>nothing found</td></tr>
                @endif

              </tbody></table>
            </div>
            <div class="text-center">
           // {{ $customers->appends(Request::all())->render()}}
            </div>
            <!-- /.box-body -->
          </div>
@stop