@extends('adminlte::page')

@section('title', 'رسید')

@section('content_header')
    <h1 class="text-center" >مه‌رزی  {{ $marz->name }}</h1>
@stop
@section('content')
<style>
  #wrapper2 {
    width: 120px;
    /*border: 1px solid black;*/
    overflow: hidden; /* will contain if #first is longer than #second */
}
#first {
    width: 50px;
    float:left; /* add this */
    /*border: 1px solid red;*/
}
#second {
  width: 60px;
    /*border: 1px solid green;*/
    overflow: hidden; /* if you don't want #second to wrap below #first */
}

  </style>
<form  action="{{ route('balance.store') }}" method="post" oninput="qty.value=parseInt(amount.value)/parseInt(unit_price.value)">
      	{{csrf_field()}}
            <div class="box-body">
              <div class="row">
              	<input type="hidden"  name="id_marz" class="form-control" value="{{ $id_marz }}">
                <div class="col-xs-2">
                  <input type="number" id="amount" name="amount" class="form-control" placeholder="بڕ">
                </div>
                <div class="col-xs-1">
                  <input type="number" id="unit_price" name="unit_price" class="form-control" placeholder="نرخی تاك">
                </div>
                <div class="col-xs-1">
                  <input type="number" id="qty" name="qty" class="form-control" placeholder="بری رسید" >
                </div>
                <div class="col-xs-2">
                  <input type="date" name="date" class="form-control" placeholder="به‌روار">
                </div>
                <div class="col-xs-2">
                  <input type="text" name="invoice_no" class="form-control" placeholder="ژ.وه‌سل">
                </div>
                <div class="col-xs-2">
                  <input type="text" name="note" class="form-control" placeholder="تێبینی">
                </div>
                <div class="col-xs-2">
                  <button type="submit" class="btn btn-block btn-lg btn-success btn-lg">زیادكردن</button>
                </div>
              </div>
            </div>
        </form>

<table class="table table-responsive table-hover table-border table-bordered">
	<thead>
		<tr>
			<th>بری پاره‌
			</th>
			<th>به‌روار
			</th>
			<th>ژ.وه‌سل
			</th>
			<th>تێبینی
			</th>
			<th>گۆرانكاری
			</th>
		</tr>
	</thead>
	<tbody>
		@foreach($balance as $b)
		<tr>
			<td>{{ $b->amount }}
			</td>
			<td>{{ $b->date }}
			</td>
			<td>{{ $b->invoice_no }}
			</td>
			<td>{{ $b->note }}
			</td>
			<td>
        <div id="wrapper2">
          <div id="first">
				    <form method="post" action="{{ route('balance.destroy',$b->id) }}" >
                                      {{ csrf_field() }} 
                                      <input type="hidden" name="_method" value="DELETE"  >
                                      <button class="btn btn-danger btn-sm" onclick="return confirm('دڵنیایت له‌ سڕینه‌وه‌؟')">سڕینه‌وه‌</button>
                                  </form>
              </div>
              <div id="second">
                 <form method="get" action="{{ route('balance.edit',$b->id) }}" >
                                      {{ csrf_field() }} 
                                      <input type="hidden" name="_method" value="PUT"  >
                                      <button class="btn btn-primary btn-sm" >نوێكردنه‌وه‌</button>
                                  </form>
              </div>
			</td>
		</tr>
		@endforeach
	</tbody>
	</table>
	@stop