@extends('adminlte::page')

@section('title', 'مه‌رز')

@section('content_header')
    <h1 class="text-center" >مه‌رزه‌كان </h1>
@stop
@section('content')
<form action="{{ route('balance.update',$balance->id) }}" method="post" oninput="qty.value=parseInt(amount.value)/parseInt(unit_price.value)">
      	{{csrf_field()}}
      	                <input type="hidden" name="_method" value="PUT">

            <div class="box-body">
              <div class="row">
              	 
                <div class="col-xs-2">
                  <input type="number" id="amount" name="amount" class="form-control" placeholder="بڕ" value="{{$balance->amount}}">
                </div>
                <div class="col-xs-1">
                  <input type="number" id="unit_price" name="unit_price" class="form-control" placeholder="نرخی تاك" value="{{$balance->unit_price}}">
                </div>
                <div class="col-xs-1">
                  <input type="number" id="qty" name="qty" class="form-control" placeholder="بری رسید"  value="{{$balance->qty}}">
                </div>
                <div class="col-xs-2">
                  <input type="date" name="date" class="form-control" placeholder="به‌روار" value="{{$balance->date}}">
                </div>
                <div class="col-xs-2">
                  <input type="text" name="invoice_no" class="form-control" placeholder="ژ.وه‌سل" value="{{$balance->invoice_no}}">
                </div>
                <div class="col-xs-2">
                  <input type="text" name="note" class="form-control" placeholder="تێبینی" value="{{$balance->note}}">
                </div>
                <div class="col-xs-2">
                  <button type="submit" class="btn btn-block btn-lg btn-success btn-lg">گۆرین</button>
                </div>
              </div>
            </div>
        </form>
@stop