@extends('adminlte::page')

@section('title', 'مه‌رز')

@section('content_header')
    <h1 class="text-center" >پاره‌دانی گومرگ</h1>
@stop
@section('content')
<form action="{{ route('gumrgpayment.update',$payment->id) }}" method="post" >
      	{{csrf_field()}}
      	                <input type="hidden" name="_method" value="PUT">

            <div class="box-body">
              <div class="row">
              	 
                <div class="col-xs-2">
                  <input type="number" id="amount" name="amount" class="form-control" placeholder="بڕ" value="{{$payment->amount}}">
                </div>
                <div class="col-xs-2">
                  <input type="date" name="date" class="form-control" placeholder="به‌روار" value="{{$payment->date}}">
                </div>
                <div class="col-xs-2">
                  <input type="text" name="invoice_no" class="form-control" placeholder="ژ.وه‌سل" value="{{$payment->invoice_no}}">
                </div>
                <div class="col-xs-2">
                  <input type="text" name="note" class="form-control" placeholder="تێبینی" value="{{$payment->note}}">
                </div>
                <div class="col-xs-2">
                  <button type="submit" class="btn btn-block btn-lg btn-success btn-lg">گۆرین</button>
                </div>
              </div>
            </div>
        </form>
@stop