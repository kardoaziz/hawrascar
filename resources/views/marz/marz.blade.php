@extends('adminlte::page')

@section('title', 'مه‌رز')

@section('content_header')
    <h1 class="text-center" >مه‌رزه‌كان</h1>
@stop
@section('content')
<style>
 #wrapper2 {
    width: 300px;
    /*border: 1px solid black;*/
    overflow: hidden; /* will contain if #first is longer than #second */
}
#first {
    width: 70px;
    float:left;
    margin:10px; /* add this */
    /*border: 1px solid red;*/
}
#second {
  width: 70px;
  margin:10px;
  float:left;
    /*border: 1px solid green;*/
     /* if you don't want #second to wrap below #first */
}
table td {
  font-size:18px;
}</style>
<!-- {{ $marz }} -->
 <form action="{{ route('marz.store') }}" method="post">
      	{{csrf_field()}}
            <div class="box-body">
              <div class="row">
                <div class="col-xs-2">
                  <input type="text" name="name" class="form-control" placeholder="ناو">
                </div>
                <div class="col-xs-2">
                  <button type="submit" class="btn btn-block btn-lg btn-success btn-lg">زیادكردن</button>
                </div>
              </div>
            </div>
        </form>
 <div class="box">
            <div class="box-header" style="text-align: center">

            </div>
<table class="table table-responsive table-hover table-bordered">
	<thead>
		<tr>
			<td class="text-center">ناو</td>
      <td class="bg-green text-center">رسید به‌رده‌ست</td>
      <td class="text-center">رسید به‌كارهاتوو</td>
      <td class="text-center">رسید</td>
      <td class="bg-green text-center">قه‌رزی گومرگ</td>
      <td class="text-center">گومرگی هاتوو</td>
      <td class="text-center">واسل به‌ گومرگ</td>
			<td>گۆرانكاری</td>
		</tr>
	</thead>
	<tbody>
		@foreach($marz as $m)
		<tr>
			<td class="text-center">{{ $m->name }}
			</td>
       <td class="bg-green text-center">{{ $m->balance - $m->total }}
      </td> 
      <td class="text-center">{{ $m->total }}
      </td>
      <td class="text-center">{{ $m->balance }}
      </td> 
      <td class="text-center bg-green">{{ $m->payment-$m->qarzgumrg }}
      </td>
      <td class="text-center">{{ $m->qarzgumrg }}
      </td>
      <td class="text-center">{{ $m->payment }}
      </td>
			<td>
				<div id="wrapper2">
          <div id="first">
				<form method="GET" action="{{ route('balance.show',$m->id) }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="submit" class="btn btn-warning  " value="رسید" >
          
                        </div>
                </form>
            </div>
            <div id="second">
                <form method="get" action="{{ route('marz.edit', $m->id) }}" >
                        {{ csrf_field() }}
       					<input type="hidden" name="_method" value="PUT"  >
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary  " value="نوێكردنه‌وه‌" >
                        </div>
                </form>
            </div>
            <div id="second">
                <form method="get" action="{{ route('gumrgpayment.show', $m->id) }}" >
                        {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT"  >
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary  " value="گومرگ" >
                        </div>
                </form>
            </div>
                              
			</td>
		</tr>
		@endforeach

	</tbody>
	</table>
</div>
	
	@stop