@extends('adminlte::page')

@section('title', 'Hawras-Cars')

@section('content_header')
    <!-- <h1>Customer View</h1> -->
@stop
@section('content')
  <!-- <h1> Gumrg Works</h1> -->
<style>
#wrapper2 {
    width: 80px;
    /*border: 1px solid black;*/
    overflow: hidden; /* will contain if #first is longer than #second */
}
#first {
    width: 40px;
    float:left; /* add this */
    /*border: 1px solid red;*/
}
#second {
  width: 40px;
    /*border: 1px solid green;*/
    overflow: hidden; /* if you don't want #second to wrap below #first */
}
</style>
<div class="box">
            <div class="box-header" style="text-align: center">

              <div class="col-sm-12"><h1>به‌شی نقل</h1></div>
                    <br><br>
                </div>
            


              <div class="table-responsive">

                <table  class="table table-hover text-nowrap table-bordered">
                <div class="row">
                </div>
                <thead>

                <tr scope="row">
                	<th >به‌رواری ده‌رچوون</th>
                	<th >به‌رواری هاتن</th>
                	<th >نقل</th>
                	<th >ژماره‌ی كاتی</th>
                	<th >ژماره‌ی شانصی</th>
                	<th >تایبه‌تمه‌ندی</th>
                	<th >ساڵ</th>
                	<th >ڕه‌نگ</th>
                	<th >جۆری ئوتومبیل</th>
                	<th >ناوی كڕیار</th>
                	<th >#</th>
                	
                </tr>
                </thead>
                <tbody>
                	@foreach($gumrg as $c)
                <tr scope="row">
	                  <td>{{$c->export_date}}</td>
	                  <td>{{$c->import_date}}</td>
	                  <td>{{$c->transfer_fee}}</td>
	                  <td>{{$c->temp_plate}}</td>
	                  <td>{{$c->specification}}</td>
	                  <td>{{$c->vin}}</td>
	                  <td>{{$c->year}}</td>
	                  <td>
                      @foreach ($colors as $co)
                          @if ($co->id==$c->color)
                              {{ $co->name }}
                          @endif
                      @endforeach
                    </td>
	                  <td>@foreach ($cars as $ca)
                        @if ($ca->id==$c->car_name)
                        {{$ca->name}}-{{$ca->company}}
                        @endif
                    @endforeach</td>
	                  <td>{{$c->customers->name}}</td>
	                  <td>{{++$i}}</td>
	                </tr>
	                @endforeach
	            </tbody>

              </table>
            <div class="text-center">
            {{ $gumrg->links() }}
            </div>
            </div>
          </div>
            </div>

            </div>

        </div>
        

            <!-- /.box-body -->
          </div>
  @stop
