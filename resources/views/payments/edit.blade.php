@extends('adminlte::page')

@section('title', 'Hawras-Cars')

@section('content_header')
    <h1 class="text-center" >گۆرانكاری له‌ پاره‌دان</h1>
@stop
@section('content')
<div class="" id="insert-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" align="center"><b>Update Payment</b></h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('payments.update',$payment->id) }}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PUT">

          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Customer</label> 
              <select class="form-control" name="customer_id">
                    @foreach($customers as $c)
                      <option value="{{$c->id}}" {{ ( $c->id == $payment->customer_id) ? 'selected' : '' }} >{{$c->name}}</option>
                  @endforeach
                  </select>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Amount</label> 
              <input type="text" name="amount" class="form-control" placeholder="Amount" value="{{$payment->amount}}">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Date</label> 
              <input type="date" name="date" class="form-control" placeholder="Date" value="{{$payment->date}}">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Note</label> 
              <input type="text" name="note" class="form-control" placeholder="Note" value="{{$payment->note}}">
            </div>
        </div>
                  <button type="submit" class="btn btn-primary">گۆرین</button>

    </form>
</div>
</div>
</div>
</div>
@stop