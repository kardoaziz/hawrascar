<style type="text/css">
        

body {
	 font-family: "Unikurd Jino";
	 width: 21cm;
  height: 29.5cm; 
  margin-left:auto;
  margin-right:auto;
  padding-left:1cm;
  padding-right:1cm;
  font-size: 15px;
  background: rgb(204,204,204); 
  -webkit-print-color-adjust: exact !important;
	}
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);

}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
  padding-bottom: 50px;
}
.logo{
	height:150px;
	width:200px;
}
.text-center{
	text-align: center;
}
.greybg{
	background-color:#C1D8CF;
	border:0px;
}
.textr{
	text-align: right;
}
.textl{
	text-align: left;
}
.textc{
	text-align: center;
}


table tr td {
	direction :rtl;
}

@media print
{     body.receipt { width: 58mm }
    .noprint, .noprint *
    {
        display: none !important;
        height:0px;
    }
    page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0cm rgba(0,0,0,0);

}
}

</style>
@php 
function convertNumberToWord($num = false)
{
    $num = str_replace(array(',', ' '), '' , trim($num));
    if(! $num) {
        return false;
    }
    $num = (int) $num;
    $words = array();
    $list1 = array('', 'یه‌ك', 'دوو', 'سێ', 'چوار', 'پێنج', 'شه‌ش', 'حه‌وت', 'هه‌شت', 'نۆ', 'ده‌', 'یانزه‌',
        'دوانزه‌', 'سیانزه‌', 'چوارده‌', 'پانزه‌', 'شانزه‌', 'حه‌ڤده‌', 'هه‌ژده‌', 'نۆزده‌'
    );
    $list2 = array('', 'ده‌', 'بیست', 'سی', 'چل', 'په‌نجا', 'شه‌ست', 'حه‌فتا', 'هه‌شتا', 'نه‌وه‌د', 'سه‌د');
    $list3 = array('', 'هه‌زار', 'ملیۆن', 'بلیۆن', 'ترلیۆن', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
    );
    $num_length = strlen($num);
    $levels = (int) (($num_length + 2) / 3);
    $max_length = $levels * 3;
    $num = substr('00' . $num, -$max_length);
    $num_levels = str_split($num, 3);
    for ($i = 0; $i < count($num_levels); $i++) {
        $levels--;
        $hundreds = (int) ($num_levels[$i] / 100);
        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' سه‌د' . ' ' : '');
        $tens = (int) ($num_levels[$i] % 100);
        $singles = '';
        if ( $tens < 20 ) {
            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
        } else {
            $tens = (int)($tens / 10);
            $tens = ' ' . $list2[$tens] . ' ';
            $singles = (int) ($num_levels[$i] % 10);
            $singles = ' ' . $list1[$singles] . ' ';
        }
        $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
    } //end for loop
    $commas = count($words);
    if ($commas > 1) {
        $commas = $commas - 1;
    }
    return implode(' ', $words);
}
@endphp
<body onload="window.print()">
<page size="A4" >

	<button onclick="window.history.back();" class="noprint" style="background-color:red;width:100px;height:50px;border-radius:30px;font-weight:bold;font-size:17px;">Close</button>
  
  <table style="width:100%;">
    
      <div style="position:fixed; left:50px;top: 35px;">NO <div style="border:1px solid grey;border-radius: 5px;min-width: 100px !important;float: right;text-align: center;">{{$payment->id}}</div> </div>
      
    <tr>
      <td colspan="6"><img style="width:100%;" src="{{ asset('imgs/invoice.png') }}">
    </td>

    </tr>
    <tr>
      <td style="border:1px solid grey;border-radius: 5px;width:100px;text-align: center;">{{$payment->date}}</td>
      <td style="width:150px;"></td>
      <td style="border:1px solid grey;border-radius: 25px;font-weight:bold;" class="text-center">پسوله‌ی پاره‌ وه‌رگرتن</td>
      <td style="width:40px;"></td>
      <td  style="border:1px solid grey;text-align: center;width:50px; background-color:#C1D8CF;color:black; font-weight:bold;">دینار</td>
      <td  style="border:1px solid grey;text-align: left;">1000000</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td style="width:40px;"></td>
      <td style="border:1px solid grey;text-align: center;width:50px;background-color:#C1D8CF;color:black; font-weight:bold;" >دۆلار</td>
      <td style="border:1px solid grey;text-align: left;" >{{$payment->amount}}</td>
    </tr>
    <tr>
      <td colspan="5" style="border-bottom:1px dashed grey;">
        @php 
        echo convertNumberToWord($payment->amount).' دۆلار'; 
        @endphp
          
        </td>
      <td> بڕی </td>
    </tr>
    <tr>
      <td colspan="5" style="border-bottom:1px dashed grey;">{{$customer->name}}</td>
      <td>وه‌رگرت له‌ به‌ڕێز</td>
    </tr>
    <tr>
      <td colspan="5" style="border-bottom:1px dashed grey;">{{$payment->note}}</td>
      <td>بۆ مه‌به‌ستی</td>
    </tr>
    <tr>
      <td colspan="6" style="border-bottom: 2px solid black;height:20px;"></td>
    </tr>
    <tr>
      <td> </td>
      <td>كۆمپانیای هوراس: </td>
      <td colspan="2"></td>
      <td>ئیمزای پێده‌ر:</td>
      <td></td>
      
    </tr>
  </table>
  <br>
  <br>
  <hr>
  <table style="width:100%;margin-top:30px;">
    
      <div style="position:fixed; left:50px;top: 450px;">NO <div style="border:1px solid grey;border-radius: 5px;min-width: 100px !important;float: right;text-align: center;">{{$payment->id}}</div> </div>
      
    <tr>
      <td colspan="6"><img style="width:100%;" src="{{ asset('imgs/invoice.png') }}">
    </td>

    </tr>
    <tr>
      <td style="border:1px solid grey;border-radius: 5px;width:100px;text-align: center;">{{$payment->date}}</td>
      <td style="width:150px;"></td>
      <td style="border:1px solid grey;border-radius: 25px;font-weight:bold;" class="text-center">پسوله‌ی پاره‌ وه‌رگرتن</td>
      <td style="width:40px;"></td>
      <td  style="border:1px solid grey;text-align: center;width:50px; background-color:#C1D8CF;color:black; font-weight:bold;">دینار</td>
      <td  style="border:1px solid grey;text-align: left;">1000000</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td style="width:40px;"></td>
      <td style="border:1px solid grey;text-align: center;width:50px;background-color:#C1D8CF;color:black; font-weight:bold;" >دۆلار</td>
      <td style="border:1px solid grey;text-align: left;" >{{$payment->amount}}</td>
    </tr>
    <tr>
      <td colspan="5" style="border-bottom:1px dashed grey;">
        @php 
        echo convertNumberToWord($payment->amount).' دۆلار'; 
        @endphp
          
        </td>
      <td> بڕی </td>
    </tr>
    <tr>
      <td colspan="5" style="border-bottom:1px dashed grey;">{{$customer->name}}</td>
      <td>وه‌رگرت له‌ به‌ڕێز</td>
    </tr>
    <tr>
      <td colspan="5" style="border-bottom:1px dashed grey;">{{$payment->note}}</td>
      <td>بۆ مه‌به‌ستی</td>
    </tr>
    <tr>
      <td colspan="6" style="border-bottom: 2px solid black;height:20px;"></td>
    </tr>
    <tr>
      <td> </td>
      <td>كۆمپانیای هوراس: </td>
      <td colspan="2"></td>
      <td>ئیمزای پێده‌ر:</td>
      <td></td>
      
    </tr>
  </table>
</page>
</body>