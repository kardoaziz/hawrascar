
@extends('adminlte::page')

@section('title', 'Hawras-Cars')

@section('content_header')
    <h1 class="text-center" >قه‌رزی كریاره‌كان</h1>
@stop
@section('content')
<style>
#wrapper2 {
    width: 80px;
    /*border: 1px solid black;*/
    overflow: hidden; /* will contain if #first is longer than #second */
}
#first {
    width: 40px;
    float:left; /* add this */
    /*border: 1px solid red;*/
}
#second {
  width: 40px;
    /*border: 1px solid green;*/
    overflow: hidden; /* if you don't want #second to wrap below #first */
}
</style>


	<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#insert-modal">
                <i class="fa fa-edit"></i>
              </button>
              <div class="col-md-4">
                <form class="form-horizontal pull-right" method="get" action="{{ route('payments.search') }}">
                    <div class="input-group">
                        <div class="input-group-btn">
                          <input type="submit" class="btn btn-danger" value="گه‌ڕان به‌ پێی ناوی كڕیار">
                        </div>
                        <!-- /btn-group -->
                        <select class="form-control" style="width: 100%; direction: rtl" name="search">
                            @foreach ($customers as $c)
                              @if (isset($selected) && $c->id==$selected)
                              <option value="{{ $c->id }}" selected>{{ $c->name }}</option> 
                                 @else 
                                 <option value="{{ $c->id }}">{{ $c->name }}</option> 
                              @endif
                            @endforeach
                          </select>
                      </div>

                </form>
              </div>
<table class="table table-respinsive table-hover table-fixed text-nowrap table-bordered">
	<thead>
		<tr class="row">
			<th>قه‌رز </th>
			<th>كۆی پاره‌دان $ </th>
			<th>كۆی نه‌قل و گومرگ </th>
			<th>كریار</th>
			<th>#</th>
			<th>Edit</th>
		</tr>
	</thead>
	<tbody>
		@foreach($gumrg as $g)
			<tr class="row">
				<td>
					@if(@isset($g->pay))
						$ {{ $g->total -$g->pay }}
					@else
					$ {{ $g->total }}
					@endif
				</td>
				<td>
					@if(@isset($g->pay))
					$ {{ $g->pay }}
					@else
						$ 0
					@endif
				</td>
				<td>
					$ {{ $g->total }}
				</td>
				<td>{{ $g->name }}
				</td>
				<td>{{ ++$i }}
				</td>
        <!--  -->
				<td><div id="wrapper2">
                        <div id="first">
                        	<form method="GET"action="{{ route('payments.show',$g->id) }}">
					        {{ csrf_field() }}
			                <button type="submit" class="btn btn-primary btn-circle btn-xs" data-toggle="modal" data-target="#edit-modal" > پاره‌دانه‌كان 
			                      <!-- <i class="fa fa-times"></i> -->
			                </button>
								    </form>
                        </div>
                        <div id="second">
                        	<!-- <form method="POST" action="/payments/{{$g->id}}">
					        {{ csrf_field() }}
					        {{ method_field('DELETE') }}

					           <button type="submit" class="btn btn-danger btn-circle btn-xs" onclick="return confirm('Are you sure?')">
					           	<i class="fa fa-times"></i>
								</button>
					    </form> -->
                        </div>
                    </div>
				</td>
				
			</tr>
		@endforeach
		<tr>
		</tr>
	</tbody>
</table>
<!-- Modals -->

<div class="modal fade" id="insert-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" align="center"><b>New Payment</b></h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('payments.store') }}" method="post">
                {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Customer</label> 
              <select class="form-control" name="customer_id">
                    @foreach($customers as $c)
                      <option value="{{$c->id}}">{{$c->name}}</option>
                  @endforeach
                  </select>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Amount</label> 
              <input type="text" name="amount" class="form-control" placeholder="Amount">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Date</label> 
              <input type="date" name="date" class="form-control" placeholder="Date">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Note</label> 
              <input type="text" name="note" class="form-control" placeholder="Note">
            </div>
        </div>
                  <button type="submit" class="btn btn-primary">زیادكردن</button>

    </form>
</div>
</div>
</div>
</div>

@stop