
@extends('adminlte::page')

@section('title', 'Hawras-Cars')

@section('content_header')
    
@stop
@section('content')
<style>
#wrapper2 {
    width: 80px;
    /*border: 1px solid black;*/
    overflow: hidden; /* will contain if #first is longer than #second */
}
#first {
    width: 40px;
    float:left; /* add this */
    /*border: 1px solid red;*/
}
#second {
  width: 40px;
    /*border: 1px solid green;*/
    overflow: hidden; /* if you don't want #second to wrap below #first */
}
</style>

<div class="box">
  <div class="box-header">
      <h1 class="text-center" >حساباتی قاصه‌ </h1>
  <div style="text-align:center" class="box-header">
	<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#insert-modal">
                <i class="fa fa-edit"></i>
              </button>
              <div class="col-md-4">
                <form class="form-horizontal pull-right" method="get" action="{{ route('qasa.search') }}">
                    <div class="input-group">
                        <div class="input-group-btn">
                          <input type="submit" class="btn btn-danger" value="گه‌ڕان به‌ پێی ناوی خاوه‌ن قاصه‌">
                        </div>
                        <!-- /btn-group -->
                        <select class="form-control" style="width: 100%; direction: rtl" name="search">
                            @foreach ($xawanQasa as $c)
                              @if (isset($selected) && $c->id==$selected)
                              <option value="{{ $c->id }}" selected>{{ $c->name }}</option> 
                                 @else 
                                 <option value="{{ $c->id }}">{{ $c->name }}</option> 
                              @endif
                            @endforeach
                          </select>
                      </div>

                </form>
              </div>
            </div>
          </div>
<table class="table table-respinsive table-hover table-fixed text-nowrap table-bordered">
	<thead>
		<tr class="row">
      <th>باقیاتی پاره‌ له‌ ناو قاصه‌ </th>
      <th>كۆی پاره‌ی ڕۆیشتوو‌ $</th>
			<th>كۆی پاره‌ی هاتوو $ </th>
			<th>خاوه‌ن قاصه‌</th>
			<th>زنجیره‌</th>
			<th>Edit</th>
		</tr>
	</thead>
    <tbody>
      @foreach ($xawanQasa as $q)
          <tr class="row">
              <td>{{ $q->intotal - $q->outtotal }}</td>
              <td> {{$q->outtotal}}</td>
              <td>{{$q->intotal}}</td>
              <td>{{ $q->name }}</td>
              <td>{{ ++$i }}</td>
              <td>$</td>
          </tr>
      @endforeach
    </tbody>
</table>
<!-- Modals -->

<div class="modal fade" id="insert-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" align="center"><b>حسابی قاسه‌</b></h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('qasa.store') }}" method="post">
                {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">خاوه‌ن قاصه‌</label> 
              <select class="form-control" name="id_xawan_qasa">
                    @foreach($xawanQasa as $c)
                      <option value="{{$c->id}}">{{$c->name}}</option>
                  @endforeach
                  </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">خاوه‌ن قاصه‌</label> 
                <select class="form-control" name="in_out">
                    <option value="IN">پاره‌ی هاتوو</option>
                    <option value="OUT">پاره‌ی ڕۆیشتوو</option>
                    </select>
              </div>
            <div class="form-group">
              <label for="exampleInputEmail1">بڕ</label> 
              <input type="text" name="amount" class="form-control" placeholder="بڕ">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">به‌روار</label> 
              <input type="date" name="date" class="form-control" placeholder="به‌روار">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">تێبینی</label> 
              <input type="text" name="note" class="form-control" placeholder="تێبینی">
            </div>
        </div>
                  <button type="submit" class="btn btn-primary">زیادكردن</button>

    </form>
</div>
</div>
</div>
</div>
<div class="box-footer">
  <div class="bg-danger" style="text-align:center;direction:rtl"><h1>كۆی گشتی پاره‌ی ناو قاصه‌ :{{$inQasa - $outQasa}} </h1></div>
</div>
</div>
@stop