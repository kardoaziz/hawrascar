{{-- \resources\views\users\index.blade.php --}}
@extends('layouts.app')

@section('title', '| Hawras-Cars')

@section('content')

<div class="col-lg-10 col-lg-offset-1">
    <h1><i class="fa fa-users"></i> User Administration <a href="{{ route('roles.index') }}" class="btn btn-default pull-right">Roles</a>
    <a href="{{ route('permissions.index') }}" class="btn btn-default pull-right">Permissions</a></h1>
    <hr>
    <div class="table-responsive">
            <h3 align="center">Total Data : <span id="total_records"></span></h3>
        <table class="table table-bordered table-striped table-bordered">

            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Date/Time Added</th>
                    <th>User Roles</th>
                    <th>Operations</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    @foreach ($users as $user)
                    <tr>
                            <td>{{ $user->name }}</td> 
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td>{{ $user->roles()->pluck('name')->implode(',') }}</td>
                            <td><a href="{{ route('users.edit',$user->id) }}"><button class="btn btn-primary pull-right">ده‌ستكاریكردن</button></a>
                              <form method="post" action="{{ route('users.destroy',$user->id) }}" >
                                {{ csrf_field() }} 
                                <input type="hidden" name="_method" value="DELETE"  >
                                <button class="btn btn-danger" onclick="return confirm('دڵنیایت له‌ سڕینه‌وه‌؟')">سڕینه‌وه‌</button>
                            </form></td>
                        </tr>
                        @endfo
                    @endforeach
                </tr>
            </tbody>

        </table>
    </div>

    <a href="{{ route('users.create') }}" class="btn btn-success">Add User</a>

</div>

@endsection

