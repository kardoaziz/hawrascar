<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('Auth/login');
});

// Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::resource('/marz',"MarzController");
Route::resource('/balance',"BalanceController");
Route::resource('/gumrgpayment',"GumrgPaymenyController");

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes();

Route::get('/customers', 'CustomerController@index')->name('customers');
Route::get('/customers/search', 'CustomerController@search')->name('customers.search');
Route::get('/gumrg/search', 'GumrgController@search')->name('gumrgs.search');
Route::get('/gumrg/search_customer', 'GumrgController@search_customer')->name('gumrgs.search_customer');
Route::get('/gumrg/insertContract', 'GumrgController@insertContract')->name('gumrgs.insertContract');
Route::get('/gumrg/showContract', 'GumrgController@showContract')->name('gumrgs.showContract');
Route::get('/gumrg/action', 'GumrgController@action')->name('gumrg.action');
Route::get('/naql', 'GumrgController@naql')->name('naql');
// Route::get('/courses', 'courseController@store')->name('courses');
// Route::get('/courses', 'Courses@index')->name('home');
Route::resource('/customers',"CustomerController");
Route::resource('/gumrgs',"GumrgController");
Route::resource('/colors',"ColorController");
Route::resource('/cars',"CarsController");
Route::resource('/payments',"PaymentController");
Route::get('/payment', 'PaymentController@search')->name('payments.search');
Route::get('/invoice/{id}', 'PaymentController@invoice')->name('payments.invoice');
Route::resource('xawanQasa', 'XawanQasaController');
Route::get('/xawanQasas', 'XawanQasaController@search')->name('xawanQasa.search');
Route::resource('qasa', 'QasaController');
Route::get('/qasas', 'QasaController@search')->name('qasa.search');
// Route::resource('/courses',"courseController");
// Auth::routes();
Route::resource('roles', 'RoleController');
Route::resource('users', 'UserController');
Route::get('/users/action', 'UserController@action')->name('users.action');

Route::resource('permissions', 'PermissionController');

Route::get('/home', 'HomeController@index')->name('home');
